<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TitleAndLink extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('kiro241@gmail.com', 'Leopays')
            ->to(
                $this->data['mail']
            )
            ->subject($this->data['title'])
            ->view('mails.title-and-link')
            ->with(['text' => $this->data['text'], 'link' => $this->data['link']]);
    }
}
