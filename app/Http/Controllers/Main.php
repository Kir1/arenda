<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AppartamentsService;
use App\Services\OtherService;
use App\Services\ResponseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Validation\Rule;

class Main extends Controller
{
    public function index(Request $request)
    {
        $array = [
            'title' => 'Аренда',
            'keywords' => '',
            'description' => '',
            'areas' => ['ул. Ярославская', 'Земский мкр.'],
        ];

        if ($request->m == '1') {
            $array['mertoList'] = ['Лужская', 'Коломская'];
        }

        return view('index', ResponseService::formingPageData($array));
    }


    public function placeAnAdPage(Request $request)
    {
        $array = [
            'title' => 'Заявка на размещение объявлений по аренде',
        ];

        return view('place-an-ad', ResponseService::formingPageData($array));
    }

    public function rentApartmentPage(Request $request)
    {
        $array = [
            'title' => 'Cдать квартиру',
            'description' => 'Аренда. Хотите сдать квартиру? Наш сайт поможет!',
        ];

        return view('rent-apartment', ResponseService::formingPageData($array));
    }

    public function rentRoomPage(Request $request)
    {
        $array = [
            'title' => 'Сдать комнату',
            'description' => 'Аренда. Хотите сдать квартиру? Наш сайт поможет!',
        ];

        return view('rent-room', ResponseService::formingPageData($array));
    }

    public function termsOfUsePage(Request $request)
    {
        $array = [
            'title' => 'Пользовательское соглашение',
            'description' => 'Настоящее Пользовательское соглашение регулирует отношения между ИП Минаев Владимир Сергеевичем и пользователем сети Интернет, возникающие при использовании интернет-ресурса keymap.ru',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Пользовательское соглашение')
        ];

        return view('terms-of-use', ResponseService::formingPageData($array));
    }

    public function personalDataProcessingPage(Request $request)
    {
        $array = [
            'title' => 'Обработка персональных данных',
            'description' => 'Обработка персональных данных',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Обработка персональных данных'),
        ];

        return view('personal-data-processing', ResponseService::formingPageData($array));
    }

    public function faqForTenantsPage(Request $request)
    {
        $array = [
            'title' => 'Частые вопросы',
            'description' => 'Ответы на часто задаваемые вопросы. Возник вопрос по аренде квартиры или комнаты? Тогда тебе сюда!',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Частые вопросы'),
        ];

        return view('faq-for-tenants', ResponseService::formingPageData($array));
    }

    public function faqForOwnersPage(Request $request)
    {
        $array = [
            'title' => 'Частые вопросы собственников',
            'description' => 'Ответы на часто задаваемые вопросы. Возник вопрос по аренде квартиры или комнаты? Тогда тебе сюда!',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Частые вопросы собственников'),
        ];

        return view('faq-for-owners', ResponseService::formingPageData($array));
    }

    public function rentalGuidePage(Request $request)
    {
        $faqList = [
            [
                'title' => 'Начало поиска',
                'id' => 'rental-faq-part1',
                'children' => [
                    [
                        'title' => 'Предварительная подготовка',
                        'id' => 'rental-faq-part1_1'
                    ],
                    [
                        'title' => 'Выбор района',
                        'id' => 'rental-faq-part1_2'
                    ],
                    [
                        'title' => 'Сезонность',
                        'id' => 'rental-faq-part1_3'
                    ],
                    [
                        'title' => 'Типы домов',
                        'id' => 'rental-faq-part1_4'
                    ],
                    [
                        'title' => 'С комиссией или без',
                        'id' => 'rental-faq-part1_5'
                    ]
                ]
            ],
            [
                'title' => 'Поиск',
                'id' => 'rental-faq-part2',
                'children' => [
                    [
                        'title' => 'Где искать',
                        'id' => 'rental-faq-part2_1'
                    ],
                    [
                        'title' => 'Сайты',
                        'id' => 'rental-faq-part2_2'
                    ],
                    [
                        'title' => 'Соцсети',
                        'id' => 'rental-faq-part2_3'
                    ],
                    [
                        'title' => 'Как отличить объявления-приманки',
                        'id' => 'rental-faq-part2_4'
                    ],
                    [
                        'title' => 'Как избежать мошенников ',
                        'id' => 'rental-faq-part2_5'
                    ],
                    [
                        'title' => 'Как защититься от мошенников',
                        'id' => 'rental-faq-part2_6'
                    ]
                ]
            ],
            [
                'title' => 'Просмотр',
                'id' => 'rental-faq-part3',
                'children' => [
                    [
                        'title' => 'Типичный просмотр',
                        'id' => 'rental-faq-part3_1'
                    ],
                    [
                        'title' => 'Чеклист про просмотре квартиры',
                        'id' => 'rental-faq-part3_2'
                    ]
                ]
            ],
            [
                'title' => 'Подписание договора',
                'id' => 'rental-faq-part4',
                'children' => [
                    [
                        'title' => 'Какие нужны документы, как их проверить',
                        'id' => 'rental-faq-part4_1'
                    ],
                    [
                        'title' => 'Договор: на что обратить внимание ',
                        'id' => 'rental-faq-part4_2'
                    ],
                    [
                        'title' => 'Расторжение по соглашению сторон ',
                        'id' => 'rental-faq-part4_3'
                    ],
                    [
                        'title' => 'Расторжение по требованию сторон',
                        'id' => 'rental-faq-part4_4'
                    ]
                ]
            ],
            [
                'title' => 'Переезд',
                'id' => 'rental-faq-part5',
                'children' => [
                    [
                        'title' => 'Собираем вещи',
                        'id' => 'rental-faq-part5_1'
                    ],
                    [
                        'title' => 'Выбираем машину',
                        'id' => 'rental-faq-part5_2'
                    ],
                    [
                        'title' => 'Сервисы для переезда под ключ',
                        'id' => 'rental-faq-part5_3'
                    ],
                ]
            ],
            [
                'title' => 'Проживание в квартире',
                'id' => 'rental-faq-part6',
                'children' => [
                    [
                        'title' => 'Общие правила',
                        'id' => 'rental-faq-part6_1'
                    ],
                    [
                        'title' => 'Если снимаете с соседями',
                        'id' => 'rental-faq-part6_2'
                    ],
                    [
                        'title' => 'Как не потерять залог',
                        'id' => 'rental-faq-part6_3'
                    ],
                ]
            ],
        ];

        $typesOfHouses = [
            ['title' => 'Сталинка',
                'text' => 'В центральных районах столицы многие здания были построены до 1930-х годов. Выбирайте квартиру в таком доме,
                если для вас важна архитектура: балкон с ажурной решеткой, французское окно, эркер и потолок 4 метра высок шанс обрести именно тут.
                Нередко встречается нестандартная планировка (например, комнаты для прислуги и кладовки, которые можно использовать в качестве гардероба).
                Недостатки тоже есть: дряхлые коммуникации становятся причиной слабого напора воды и засоров. Цена аренды высока, в основном из-за того, что старинные дома чаще всего расположены в центре.',
                'img' => url('img/stalinka.png')],
            ['title' => 'Хрущевка', 'text' => 'Сталинские дома строились с 1930-х до конца 1950-х годов и долгое время считались эталоном жилого строительства.
            Неудивительно: площадь двушки в сталинке в среднем 60 м2 (для сравнения, в хрущевке всего 38 м2), просторные кухни, высокие потолки 3,2 - 4 метра,
            большие окна, широкие лестницы в подъездах. Толстые стены, поэтому отличная тепло - и звукоизоляция. Но в домах ниже 6 этажей может не быть лифта
            и мусоропровода, а цена аренды на жилье в сталинских домах выше средней.',
                'img' => url('img/hrushovka.png')],
            ['title' => 'Панельный дом', 'text' => 'Строятся с 1960-х годов и вплоть до настоящего времени, встречаются повсеместно.
            Планировка бывает хороша: однушки с просторными кухнями, изолированные комнаты в двушках и трешках, но потолки низкие и санузел часто совмещенный.
             Практически в каждой квартире выше 2 этажа есть балкон и/или лоджия. Цена аренды часто ниже, чем за равное по площади жилье в монолитном или кирпичном доме.
              Главный минус — никудышная шумо- и теплоизоляция, зимой помещение тяжело прогреть. Также из-за щелей между панелями в квартиру может попадать влага,
              поэтому советуем при осмотре проверить, нет ли в углах плесени. На верхних этажах часто бывает слабый напор воды.',
                'img' => url('img/panel-house.png')],
            ['title' => 'Монолитный', 'text' => 'Такие дома начали строить в 1990-х годах и продолжают до сих пор: к этому типу относится большинство современных новостроек.
            Достоинства: хорошая теплоизоляция, а внешние стены неплохо поглощают шум с улицы. В квартирах изолированные комнаты, раздельный санузел, балкон и/или лоджия,
            встречаются потолки 2,8-3,2 метра. А вот многоэтажность можно назвать недостатком — чем больше народу, тем меньше парковочных мест во дворе и длиннее очередь
             в районный детский сад. Монолитные здания обычно строят целыми жилыми комплексами, чаще в отдаленных от центра районах.',
                'img' => url('img/monolit-house.png')]
        ];

        $waysProtectScams = [
            [
                'title' => 'Информационные агентства',
                'text' => ' <p>Вы нашли хорошую квартиру по выгодной цене, позвонили по объявлению и получили предложение
                            приехать в офис подписать эксклюзивный договор. На месте убедительные «агенты» показывают
                            вам целый список отличных и недорогих квартир, вы подписываете договор, платите деньги,
                            получаете адреса и едете «на просмотр». В итоге выясняется, что никакого жилья по данному
                            адресу не сдается.</p>

                            <p>Вернуть деньги вам не удастся, ведь вы сами подписали контракт на оказание информационных услуг.
                             То есть, заплатили за то, что вам просто выдали адрес. Часто это прописано в документах мелким шрифтом,
                              и мало кто удосуживается внимательно все прочитать.</p>
                            '
            ],
            [
                'title' => 'Сдается квартира, выставленная на продажу',
                'text' => 'Вообще порядочный риелтор обязан сообщить, что квартира продается. К сожалению, многие этого не делают.
                Вы платите комиссию, спокойно заселяетесь, а спустя пару месяцев с вами расторгают договор на законных основаниях
                (любая из сторон имеет право расторгнуть контракт по предварительному уведомлению за месяц). Перед тем, как подписывать документы,
                 убедитесь, что собственник не собирается продавать квартиру в обозримом будущем.'
            ],
            [
                'title' => 'Сдача посуточной квартиры на длительный срок',
                'text' => 'Злоумышленники снимают квартиру, которая сдается посуточно, делают фотографии и размещают объявление о том, что она сдается на длительный срок.
                Вы приезжаете на просмотр, подписываете договор, вам даже показывают какие-то документы, отдают ключи и оставляют в квартире.
                А на следующий день появляется настоящий собственник, который, оказывается, сдавал жилье на три дня и вообще не вам.'
            ],
            [
                'title' => 'Одна и та же квартира сдается дважды',
                'text' => 'Потенциальную жертву ловят на привлекательное объявление с выгодным ценником, очень торопят при подписании документов, в конце даже дают ключи.
                А потом оказывается, что ключи и договор аренды есть не только у вас, но и других незадачливых арендаторов.'
            ],
            [
                'title' => 'Платный просмотр',
                'text' => 'Никаких хитрых схем: риелтор просто берет с вас деньги за просмотр квартиры. Сумма варьируется от тысячи до пяти тысяч рублей.
                Особо предприимчивые умудряются десятки раз показывать за небольшие деньги одну и ту же квартиру, обманывая и хозяев («Пока не нашлось приличных жильцов»),
                и арендаторов («Сдали другим претендентам»).'
            ],
            [
                'title' => 'Фальшивый адрес',
                'text' => 'Риелтор предлагает заключить договор не в квартире, а в офисе агентства (фиктивного или
                            настоящего). При заполнении документов намеренно делается ошибка в ваших паспортных данных
                            или адресе квартиры. Вам даже выдают фальшивые ключи. Когда обман вскроется, вы не сможете
                            ничего доказать: договор заключен с другим человеком или по поводу другой квартиры.
                            Старайтесь не заключать договор за пределами квартиры, которую собираетесь снимать, и
                            обязательно тщательно проверяйте все, что туда вписывает посредник.'
            ],
        ];

        $array = [
            'title' => 'Как снять жилье в Москве?',
            'description' => 'Аренда квартир без риэлторов, покупка и продажа вещей, поиск домашних услуг',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Как снять жилье в Москве?'),
            'faqList' => $faqList,
            'typesOfHouses' => $typesOfHouses,
            'waysProtectScams' => $waysProtectScams,
        ];

        return view('rental-guide', ResponseService::formingPageData($array));
    }

    public function catalogPage(Request $request)
    {
        $array = [
            'title' => 'Каталог квартир для аренды',
            'description' => '',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Поиск'),
            'apartments' => [
                [
                    'id' => 1,
                    'owner_id' => 1,
                    'name' => 'Шикарная двушка',
                    'status_id' => AppartamentsService::APPARTAMENT_STATUS_ID__CHECKED_BY_MODERATOR,
                    'number_of_rooms' => 2,
                    'area' => 144,
                    'floor' => 1,
                    'images' => [
                        OtherService::urlImageStub(),
                        OtherService::urlImageStub(),
                        OtherService::urlImageStub()
                    ],
                    'metros' => [
                        [
                            'name' => 'Филевская линия',
                            'color_code' => 'rgb(26, 193, 243)'
                        ],
                        [
                            'name' => 'Монорельс',
                            'color_code' => 'rgb(69, 184, 94)'
                        ]
                    ]
                ],
                [
                    'id' => 2,
                    'owner_id' => 1,
                    'name' => 'Однокомнатная квартира',
                    'status_id' => AppartamentsService::APPARTAMENT_STATUS_ID__POPULATED,
                    'number_of_rooms' => 1,
                    'date_of_eviction' => date("d.m.Yг.", strtotime('2020-07-12')),
                    'area' => 100,
                    'floor' => 2,
                    'images' => [
                        OtherService::urlImageStub()
                    ],
                    'metros' => [
                        [
                            'name' => 'Филевская линия',
                            'color_code' => 'rgb(26, 193, 243)'
                        ]
                    ]
                ],
            ],
            'sort_filters' => [
                ['name' => 'По дата добавления', 'id' => AppartamentsService::APPARTAMENT_SORT_FILTER_ID__PUBLISH_DATE],
                ['name' => 'По цене', 'id' => AppartamentsService::APPARTAMENT_SORT_FILTER_ID__PRICE],
                ['name' => 'По площади', 'id' => AppartamentsService::APPARTAMENT_SORT_FILTER_ID__AREA],
            ]
        ];

        return view('catalog', ResponseService::formingPageData($array));
    }

    public function ratesPage(Request $request)
    {
        $breadcrumps = OtherService::breadcrumpArray('Тарифы', [['text' => 'Сдать недвижимость', 'url' => route('create-room-get')]]);
        $array = [
            'title' => 'Тарифы',
            'description' => '',
            'breadcrumbs' => $breadcrumps,
            'rates' => [
                [
                    'name' => 'Обычная продажа',
                    'days' => 3,
                    'price' => 400,
                    'features' => [
                        ['text' => 'Объявление выделено цветом', 'check' => true],
                        ['text' => 'Вверху списка результатов поиска', 'check' => true],
                        ['text' => 'Отображается на <br> главной странице', 'check' => false],
                        ['text' => 'Реклама в социальных сетях', 'check' => false],
                        ['text' => 'Размещение на 30 <br> партнерских площадках', 'check' => false],

                    ]
                ],
                [
                    'name' => 'Быстрая продажа',
                    'days' => 7,
                    'price' => 800,
                    'popular' => true,
                    'features' => [
                        ['text' => 'Объявление выделено цветом', 'check' => true],
                        ['text' => 'Вверху списка результатов поиска', 'check' => true],
                        ['text' => 'Отображается на <br> главной странице', 'check' => true],
                        ['text' => 'Реклама в социальных сетях', 'check' => true],

                    ]
                ],
                [
                    'name' => 'Обычная продажа',
                    'days' => 14,
                    'price' => 1200,
                    'features' => [
                        ['text' => 'Объявление выделено цветом', 'check' => true],
                        ['text' => 'Вверху списка результатов поиска', 'check' => true],
                        ['text' => 'Отображается на <br> главной странице', 'check' => true],
                        ['text' => 'Реклама в социальных сетях', 'check' => true],
                        ['text' => 'Размещение на 30 <br> партнерских площадках', 'check' => true],
                    ]
                ],
            ]
        ];

        return view('rates', ResponseService::formingPageData($array));
    }

    public function createRoomPage(Request $request)
    {
        $array = [
            'title' => 'Разместить объявление',
            'keywords' => '',
            'description' => '',
            'breadcrumbs' => OtherService::regularBreadcrumpArray('Сдать квартиру')
        ];
        return view('create-update-room', ResponseService::formingPageData($array));
    }

    public function createRoom(Request $request)
    {
        $this->validate($request, [
            "photos" => 'required|array|min:1|max:10',
            "photos.*" => 'required|image|max:1000|dimensions:min_width=800',
        ]);

        return '';
    }

    public function updateRoomPage(Request $request)
    {
        return '';
    }

    public function repeatUpdateRoomPage(Request $request)
    {
        return '';
    }

    public function roomPage(Request $request)
    {
        return '';
    }

    public function auth(Request $request)
    {
        return UserService::getUserMinifinedById();
    }

    public function getCodeForSMS(Request $request)
    {
        $this->validate($request, [
            'phone' => [
                'required',
                Rule::notIn(['8-666-666-66-66']),
            ],
        ]);

        if ($request->phone == '8-777-777-77-77') return ResponseService::error();

        return '';
    }
}
