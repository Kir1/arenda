<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\FileService;
use App\Services\ResponseService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function changeAvatar(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image|max:1000|dimensions:max_width=200,max_height=200'
        ]);

        $user = Auth::user();
        Storage::disk('public')->delete($user->avatar);
        $pathAvatar = FileService::saveImage('avatar', 'avatars', ['width' => 133]);
        $user->avatar = $pathAvatar;
        $user->save();

        return '';
    }

    public function getProfile(Request $request)
    {
        return UserService::getUserMinifinedById();
    }

    public function index(Request $request, $id)
    {
        $array = [
            'breadcrumbs' => [
                ['text' => 'Главная', 'url' => route('home')],
                ['text' => 'Личный кабиент', 'url' => route('profile-page', ['id' => $id])],
                ['text' => 'Личные данные'],
            ],
            'title' => 'Профиль'
        ];
        return view('profile', ResponseService::formingPageData($array));
    }

    public function savePhone(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|regex:/8-\d{3}-\d{3}-\d{2}-\d{2}/',
        ]);
        $user = Auth::user();
        $user->name = $request->phone;
        $user->save();
        return '';
    }

    public function saveName(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:2|max:15',
        ]);
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();
        return '';
    }

    public function savePassword(Request $request)
    {
        $array = [
            'oldPassword' => (string)$request->oldPassword,
            'newPassword' => (string)$request->newPassword,
            'newPassword_confirmation' => (string)$request->newPassword_confirmation,
        ];

        $user = Auth::user();

        Validator::make($array, [
            'oldPassword' => [
                'required', 'string', 'min:6', 'max:10',
                function ($attribute, $value, $fail) use ($user, $array) {
                    if (Hash::check($array['oldPassword'], $user->password) === false) $fail('Старый пароль не совпадает с текущим');
                },
            ],
            'newPassword' => 'required|string|min:6|max:10|confirmed',
        ])->validate();

        $user->password = Hash::make($array['newPassword']);
        $user->save();

        return '';
    }

    public function saveEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|min:5|max:40',
        ]);
        $user = Auth::user();
        $user->email = $request->email;
        $user->save();
        return '';

        return '';
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'image|max:1000|dimensions:max_width=200,max_height=200',
            'phone' => 'regex:/8-\d{3}-\d{3}-\d{2}-\d{2}/',
            'name' => 'string|min:2|max:15',
            'password' => 'string|min:6|max:10|confirmed',
            'email' => 'email|min:5|max:40',
        ]);

        $user = Auth::user();
        if ($request->avatar) {
            Storage::disk('public')->delete($user->avatar);
            $pathAvatar = FileService::saveImage('avatar', 'avatars', ['width' => 133]);
            $user->avatar = $pathAvatar;
        }
        if ($request->phone) $user->phone = $request->phone;
        if ($request->name) $user->name = $request->name;
        if ($request->password) $user->password = Hash::make($request->password);
        if ($request->email) $user->email = $request->email;

        $user->save();

        return '';
    }

    public function governmentServicesBinding(Request $request)
    {
        $this->validate($request, [
            'oldPassword' => [
                'required',
            ],
        ]);
        return '';
    }

    public function governmentServicesBindingCancel(Request $request)
    {
        return '';
    }

    public function fbBinding(Request $request)
    {
        return '';
    }

    public function fbBindingCancel(Request $request)
    {
        return '';
    }

    public function vkBinding(Request $request)
    {
        return '';
    }

    public function vkBindingCancel(Request $request)
    {
        return '';
    }

    public function instagramBinding(Request $request)
    {
        return '';
    }

    public function instagramBindingCancel(Request $request)
    {
        return '';
    }

}
