<?php

namespace App\Http\Controllers\Auth;

use App\Services\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Services\ResponseService;

class RegisterController extends Controller
{
    public function registerByEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|min:5|max:40|unique:users',
            'password' => 'required|string|min:6|max:10',
        ]);
        $user = UserService::createUser($request->email, $request->password);
        Auth::loginUsingId($user->id);
        return '';
    }

    public function registerByPhone(Request $request)
    {
        $SMSCode = (int)str_replace('-', '', $request->SMSCode);

        Validator::make(['SMSCode' => $SMSCode], [
            'SMSCode' => [
                'required',
                Rule::notIn([1111]),
            ],
        ])->validate();

        if ($SMSCode == 2222) return ResponseService::error();

//        if ($SMSCode == 3333) {
//            $banRequestGetSMSCode = session('banRequestGetSMSCode', '');
//            if ($banRequestGetSMSCode) return ResponseService::fieldsErrors(['SMSCode' => ['Вы забанены и сможете повторить запрос только через 24 часа.']]);
//            $countCheckSMSCode = session('countCheckSMSCode', 3);
//            if ($countCheckSMSCode == 1) {
//                session(['banRequestGetSMSCode' => true]);
//                return ResponseService::fieldsErrors(['SMSCode' => ['У вас закончились попытки и вы забанены на 24 часа.']]);
//            } else {
//                session(['countCheckSMSCode' => $countCheckSMSCode--]);
//                return response()->json(['message-code-sms' => "У вас осталось $countCheckSMSCode попыток"], 200);
//            }
//        }

        return '';
    }

    public function registerPage()
    {
        $array = ['title'=> 'Регистрация','keywords'=> 'регистрация на сайте', 'description' => 'Регистрация на сайте'];
        return view('index', ResponseService::formingPageData($array));
    }
}
