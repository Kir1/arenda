<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\ResponseService;
use App\Services\TokenService;
use App\Mail\TitleAndLink;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    public function index()
    {
        $array = ['title' => 'Восстановление пароля', 'keywords' => 'восстановление пароля', 'description' => 'Восстановление пароля на сайте'];
        return view('index', ResponseService::formingPageData($array));
    }

    public function confirmationPage(Request $request)
    {
        $token = TokenService::getTokenByToken($request->token);
        if ($token) {
            $array = ['title' => 'Потверждение пароля', 'token' => $request->token];
            return view('index', ResponseService::formingPageData($array));
        } else abort(404);
    }

    public function forgot(Request $request)
    {
        $email = $request->email;
        $this->validate($request, [
            'email' => [
                'required',
                'email',
                'max:20',
                function ($attribute, $value, $fail) use ($email) {
                    $user = User::where('email', $email)->first();
                    if ($user === null) $fail('Указанной почты не существует');
                },
            ],
        ]);

        $token = TokenService::createToken($request->email);

        Mail::send(new TitleAndLink([
            'title' => 'Восстановление пароля',
            'text' => 'Пройдите по ссылке для потверждения и смены пароля',
            'link' => route("recovery-password-confirmation-page", ["token" => $token->token]),
            'mail' => $request->email
        ]));

        return '';
    }

    public function confirmation(Request $request)
    {
        $user = null;
        $token = TokenService::getTokenByToken($request->token);
        if ($token) {
            $user = User::where('email', $token->email)->first();
            if ($user) {
                $user->email_verified_at = Carbon::now()->toDateTimeString();
                $user->password = Hash::make($request->password);
                $user->save();
                $token->delete();
                Auth::loginUsingId($user->id);
                return '';
            } else return ResponseService::error('Пользователя не существует');
        } else  return ResponseService::error('Токена не существует');
    }
}
