<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LoginController extends Controller
{
    public function loginByEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|min:5|max:40',
            'password' => 'required|string|min:6|max:10',
        ]);
        if (Auth::attempt($request->only('email', 'password'))) return '';;
        return ResponseService::fieldsErrors([
            'email' => ['Почта или пароль неправильные'],
            'password' => ['Почта или пароль неправильные'],
        ]);
    }

    public function loginByPhone(Request $request)
    {
        $SMSCode = (int)str_replace('-', '', $request->SMSCode);

        Validator::make(['SMSCode' => $SMSCode], [
            'SMSCode' => [
                'required',
                Rule::notIn([1111]),
            ],
        ])->validate();

        if ($SMSCode == 2222) return ResponseService::error();

//        if ($SMSCode == 3333) {
//            $banRequestGetSMSCode = session('banRequestGetSMSCode', '');
//            if ($banRequestGetSMSCode) return ResponseService::fieldsErrors(['SMSCode' => ['Вы забанены и сможете повторить запрос только через 24 часа.']]);
//            $countCheckSMSCode = session('countCheckSMSCode', 3);
//            if ($countCheckSMSCode == 1) {
//                session(['banRequestGetSMSCode' => true]);
//                return ResponseService::fieldsErrors(['SMSCode' => ['У вас закончились попытки и вы забанены на 24 часа.']]);
//            } else {
//                session(['countCheckSMSCode' => $countCheckSMSCode--]);
//                return response()->json(['message-code-sms' => "У вас осталось $countCheckSMSCode попыток"], 200);
//            }
//        }

        return '';
    }

    public function recoveryPassword()
    {
        return '';
    }

    public function logout()
    {
        Auth::logout();
        return '';
    }

    public function logoutPage()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function loginPage()
    {
        $array = ['title'=> 'Вход','keywords'=> 'вход на сайт', 'description' => 'Вход на сайт'];
        return view('index', ResponseService::formingPageData($array));
    }
}
