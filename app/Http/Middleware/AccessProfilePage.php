<?php

namespace App\Http\Middleware;

use App\Services\ProfileService;
use Closure;

class AccessProfilePage
{
    public function handle($request, Closure $next)
    {
        if (ProfileService::checkProfileAccess()) return $next($request);
        abort(404);
    }
}
