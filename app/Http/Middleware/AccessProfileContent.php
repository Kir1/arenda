<?php

namespace App\Http\Middleware;

use App\Services\ProfileService;
use App\Services\ResponseService;
use Closure;

class AccessProfileContent
{
    public function handle($request, Closure $next)
    {
        if (ProfileService::checkProfileAccess()) return $next($request);
        return ResponseService::error403();
    }
}
