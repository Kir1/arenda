<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class ProfileService
{
    public static function checkProfileAccess()
    {
        $request = request();
        $user = Auth::user();
        if ($user) return $user->id == $request->id;
        else return false;
    }
}
