<?php

namespace App\Services;

use Illuminate\Support\Facades\Route;

class ResponseService
{
    public static function response($message = 'Запроc прошёл успешно', $code = 200, $fields = [])
    {
        return response()->json(array_merge([
            'code' => $code,
            'message' => $message
        ], $fields), $code);
    }

    public static function response201()
    {
        return ResponseService::response('Создание прошло успешно', 201);
    }

    public static function error($message = 'Ошибка на сервере.', $code = 500, $fields = [])
    {
        return ResponseService::response($message, $code, $fields);
    }

    public static function fieldsErrors($errors)
    {
        return ResponseService::error('Ошибки полей', 500,
            [
                'message' => 'Ошибки полей',
                'errors' => $errors
            ]
        );
    }

    public static function error403()
    {
        return ResponseService::error('Нет доступа', 403);
    }

    /**
     * сервис соц сети для слайдера на главной
     *
     * @param string $imageUrl название картинки с разрешением. Путь будет построен сам.
     * @param string $name название сервиса
     * @param double $rating рейтинг, от 0 до 5, может быть с одним знаком после запятой, прим. 0.5
     * @param integer $count_of_reviewsмассив количество проголосовавших
     */
    public static function createSocNetService(string $imageUrl, string $name, float $rating, int $count_of_reviews)
    {
        return [
            'img' => FileService::getImgUrl($imageUrl.'.png'),
            'name' => $name,
            'rating' => $rating,
            'count_of_reviews' => $count_of_reviews
        ];
    }

    /**
     * хелпер, который добавляет для get страниц спец данные
     *
     * @param array $array массив с данными которым будет добавлены
     */
    public static function formingPageData(array &$array = [])
    {

        $user = UserService::getUserMinifinedById();
        $array['user'] = $user;
        $array['cities'] = GeoService::CITIES;
        $array['pageRouteNameForOpenModal'] = Route::currentRouteName();
        $array['socnetServices'] = [
            ResponseService::createSocNetService('icon-yell', 'Yell', 4.4, 100),
            ResponseService::createSocNetService('icon-flamp', 'Flamp', 4.8, 100),
            ResponseService::createSocNetService('icon-google', 'Google', 4.6, 141),
            ResponseService::createSocNetService('icon-app-store', 'AppStore', 4.1, 699),
        ];
        $array['mainJson'] = json_encode($array);
        return $array;
    }
}
