<?php

namespace App\Services;

use App\Models\Token;
use Illuminate\Support\Facades\Hash;

class TokenService
{
    const ID_TOKEN_TYPE_PASSWORD_RECOVERY = 1;

    public static function createToken($email, $type_id = self::ID_TOKEN_TYPE_PASSWORD_RECOVERY)
    {
        $token = str_replace("/", "", Hash::make($email));
        return Token::create(
            [
                'email' => $email,
                'type_id' => $type_id,
                'token' => $token
            ]);
    }

    public static function getTokenByToken($token, $type_id = self::ID_TOKEN_TYPE_PASSWORD_RECOVERY)
    {
        return Token::where([
            ['token', '=', $token],
            ['type_id', '=', $type_id]
        ])->first();
    }
}
