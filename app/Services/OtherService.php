<?php

namespace App\Services;

class OtherService
{
    public static function regularBreadcrumpArray($title)
    {
        return [
            ['text' => 'Главная', 'url' => route('home')],
            ['text' => $title],
        ];
    }

    public static function breadcrumpArray($title, $array = [])
    {
        $array2 = new \ArrayObject($array);
        $array2 = $array2->getArrayCopy();
        array_unshift($array2, ['text' => 'Главная', 'url' => route('home')]);
        $array2 [] = ['text' => $title];
        return $array2;
    }

    public static function urlImageStub()
    {
        return url('/img/not_img.jpg');
    }
}
