<?php

namespace App\Services;

class GeoService
{
    const CITY_ID_MOSCOW = 1;
    const CITY_ID_IRKUTSK = 2;
    const CITY_ID_NIZHNY_NOVGOROD = 3;

    const CITIES = [
        [
            'name' => 'Москва',
            'id' => self::CITY_ID_MOSCOW,
            'domain' => 'moscow'
        ],
        [
            'name' => 'Иркутск',
            'id' => self::CITY_ID_IRKUTSK,
            'domain' => 'irkutsk'
        ],
        [
            'name' => 'Нижний Новгород',
            'id' => self::CITY_ID_NIZHNY_NOVGOROD,
            'domain' => 'nizhny-novgorod'
        ],
    ];

    public static function getCity($id)
    {
        foreach (self::CITIES as $entity) if ($id == $entity['id']) return $entity;
        return null;
    }
}
