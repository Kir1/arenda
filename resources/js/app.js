require('./bootstrap');

/**
 * vuetify polyfil
 */
import 'babel-polyfill';

/**
 *
 */
window.Vue = require('vue');

/**
 * vuetify
 */
// import Vuetify from 'vuetify';
//
// Vue.use(Vuetify);
//
// const vuetify = new Vuetify();


/**
 * vuex
 */
import Vuex from 'vuex';

Vue.use(Vuex);
import {store as storeIni} from "./store";

const store = new Vuex.Store(storeIni);

/**
 * vue-router
 */
// import VueRouter from 'vue-router';
// import {routes} from './routes/routes';
// import {navigathionHook} from "./routes/hook";
//
// Vue.use(VueRouter);
// const router = new VueRouter({
//     mode: 'history',
//     routes
// });
//
// navigathionHook(router, store);

/**
 * vue-select
 */
import vSelect from "vue-select";

Vue.component("v-select", vSelect);

vSelect.props.components.default = () => ({
    OpenIndicator: {
        render: createElement => createElement('span', '1')
    },
});

/**
 * vue-mask
 */
import VueTheMask from 'vue-the-mask';

Vue.use(VueTheMask);

/**
 * custom filters
 */
Vue.filter('kb', val => {
    return Math.floor(val/1024);
});

/**
 */
import {getAbsoluteUrl, getUrlDefaultImage, getUrlImage1, getUrlImage2} from "./helpers/general";

const mainData = JSON.parse(document.getElementById('main-json').getAttribute('data-json'));

import {USER_INI} from "./store/modules/user";

store.commit(USER_INI, mainData.user);

const mixinComponent = {
    provide: function () {
        return {
            getDataForChildren: this.getDataForChildren,
            getAbsoluteUrl: this.getAbsoluteUrl
        };
    },
    methods: {
        getDataForChildren() {
            return this.getData();
        },
        getAbsoluteUrl(url) {
            return getAbsoluteUrl(url);
        },
        getData() {
            return {};
        },
    },
}

import PageProfile from './components/pages/PageProfile';
import PageCreateUpdateRoom from './components/pages/page-create-update-room/PageCreateUpdateRoom';

new Vue({
    el: '#app',
    store,
    components: {PageProfile, PageCreateUpdateRoom},
    provide: function () {
        return {
            getAbsoluteUrl,
            getUrlDefaultImage,
            getUrlImage1,
            getUrlImage2
        };
    },
});

import UserHeader from "./components/user-header/UserHeader";

new Vue({
    el: '#header-user',
    store,
    components: {UserHeader},
    template: '<user-header/>',
    provide: function () {
        return {
            getData: this.getData,
            getAbsoluteUrl: this.getAbsoluteUrl,
        };
    },
    methods: {
        getData() {
            let obj = {
                pageRouteNameForOpenModal: mainData.pageRouteNameForOpenModal,
            };
            if (mainData.hasOwnProperty('token')) obj.token = mainData.token;
            return obj;
        },
        getAbsoluteUrl(url) {
            return getAbsoluteUrl(url);
        },
    },
});

import PageCatalog from "./components/pages/PageCatalog";

// if (document.getElementById('catalog-page')) new Vue({
//     el: '#catalog-page',
//     store,
//     components: {PageCatalog},
//     template: '<page-catalog/>',
//     provide: function () {
//         return {
//             getData: this.getData,
//             getAbsoluteUrl: this.getAbsoluteUrl,
//         };
//     },
//     methods: {
//         getData() {
//             return {
//                 apartments: mainData.apartments,
//                 sortFilters: mainData.sort_filters
//             };
//         },
//         getAbsoluteUrl(url) {
//             return getAbsoluteUrl(url);
//         },
//     },
// });

[...document.getElementsByClassName('change-city')].forEach(parent => {
    var divText = null;
    for (var i = 0; i < parent.childNodes.length; i++) {
        if (parent.childNodes[i].classList && parent.childNodes[i].classList.contains('change-city-select-text')) {
            divText = parent.childNodes[i];
            break;
        }
    }
    if (divText) for (var i = 0; i < parent.childNodes.length; i++) {
        if (parent.childNodes[i].classList && parent.childNodes[i].classList.contains('change-city-select')) {
            parent.childNodes[i].addEventListener("change", (event) => {
                divText.innerHTML = event.target.options[event.target.selectedIndex].text;
                // location.href = window.location.protocol + '//'
                //     + event.target.value
                //     + '.'
                //     + window.location.hostname;
            });
        }
    }
});

//////////

