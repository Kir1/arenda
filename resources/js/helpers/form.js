export function createFieldFormOnyFields(field) {
    return {
        field,
        edit: false,
        validate: statusNoValidate,
        errors: []
    }
}

export function getNameAndExtFile(file) {
    let name = file.name;
    let lastDot = name.lastIndexOf('.');
    let fileName = name.substring(0, lastDot);
    let ext = name.substring(lastDot + 1);
    return {name: fileName, ext};
}

export function checkFileImage(file) {
    return file && file['type'].split('/')[0] === 'image';
}
