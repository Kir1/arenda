import {checkFileImage} from "../../helpers/form";

export const FIELD_STATUS__NO_VALIDATE = 0;
export const FIELD_STATUS__SUCCESS = 1;
export const FIELD_STATUS__ERRORS = 2;

export const REQUEST_STATUS__NOT_PUSH = 0;
export const REQUEST_STATUS__PUSH = 1;
export const REQUEST_STATUS__SUCCESS = 2;
export const REQUEST_STATUS__ERROR = 3;

export const main = {
    /*
    структура  data может быть произвольной,
    данный миксин валидирует поле формы на фронтенде
    метод миксина принимает и модифицирует клон поля для кода извне
    (заменяет значение и добавляет, убирает ошибки фронтенда)
    у клона поля обязательно должно быть свойство frontendErrors(объект, где имя свойства - это имя ошибки, а значение - текст ошибки)
    а ещё свойство value и свойство validateStatus(значение из контстант выше, по дефолту FIELD_STATUS__NO_VALIDATE)
    */
    methods: {
        changeFieldWithValidate(field, value, validate = null) {
            field = {...field};
            field.value = value;
            if (validate) validate(field);
            let errors = Object.values(field.frontendErrors).filter(function (el) {
                return el !== null;
            });
            if (errors.length > 0) field.validateStatus = FIELD_STATUS__ERRORS;
            else field.validateStatus = FIELD_STATUS__SUCCESS;
            return field;
        },

        frontendError(field, nameErrorField, value, checkFunction) {
            //активируем либо деактивируем ошибку(и создаём, если валидации ещё не было)
            if (checkFunction())
                field.frontendErrors[nameErrorField] = value;
            else
                field.frontendErrors[nameErrorField] = null;
        },

        validateFieldRequired(field) {
            this.frontendError(field, 'required', 'Поле обязательно',
                () => field.value === undefined || field.value === null || field.value === ''
            );
        },
        validateFieldEmail(field) {
            this.frontendError(field, 'email', 'Почта невалидна', () => {
                const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return pattern.test(field.value) === false;
            });
        },
        validateFieldPhone(field) {
            this.frontendError(field, 'phone', 'Введите номер телефона полностью', () => {
                const pattern = /8-\d{3}-\d{3}-\d{2}-\d{2}/;
                return pattern.test(field.value) === false;
            });
        },
        //maxSize в байтах, там это 1 мб
        changeAndValidateFieldImageWithPromise(field, value, maxSize = 1000000, sizeText = '1 МБ', maxWidth = 200, maxHeight = 200) {
            field = this.changeFieldWithValidate(field, value);
            return new Promise((resolve, reject) => {
                if (field.value === null) resolve(field);

                this.frontendError(field, 'fileSize', 'Файл должен весить ' + sizeText, () => {
                    return field.value.size > maxSize;
                });

                this.frontendError(field, 'fileImage', 'Файл должен быть картинкой', () => {
                    return checkFileImage(field.value) == false;
                });

                let errorDimension = 'Картинка должна быть не больше размеров ' + maxWidth + 'x' + maxHeight;
                let promise = new Promise((resolve, reject) => {
                    var reader = new FileReader;
                    reader.readAsDataURL(field.value);
                    reader.onload = function (e) {
                        let result = {check: false, field};
                        if (checkFileImage(field.value)) {
                            var img = new Image();
                            field.url = e.target.result;
                            img.src = e.target.result;
                            img.onload = function () {
                                let w = img.width;
                                let h = img.height;
                                result.check = w > maxWidth || h > maxHeight;
                                resolve(result);
                            }
                        } else resolve(result);
                    };
                });
                promise.then(
                    (result) => {
                        this.frontendError(result.field, 'imageDimension', errorDimension, () => {
                            return result.check;
                        });
                        resolve(field);
                    }
                );
            });
        },
        validateTextFieldMaxLength(field, max = 20) {
            this.frontendError(field, 'maxSymbol', `Максимум ${max} символов`, () => field.value.length > max);
        },
        validateTextFieldMinLength(field, min = 6) {
            this.frontendError(field, 'minSymbol', `Минимум ${min} символов`, () => field.value.length < min);
        },
        validateSMSCodeFieldMinLength(field, min = 4) {
            let value = field.value.replace(/-/g, '');
            this.frontendError(field, 'minSymbolSMSCode', `Минимум ${min} символов`, () => value.length < min);
        },
        validateTextFieldPhone(field) {
            this.frontendError(field, 'phone', `Введите номер телефона правильно`, () => field.value.length > max);
        },
    },
}
