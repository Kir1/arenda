import Errors from "../../../components/general/Errors";
import {FIELD_STATUS__ERRORS, FIELD_STATUS__NO_VALIDATE, FIELD_STATUS__SUCCESS} from "../main";

export const binding = {
    data() {
        return {
            binding: false,
            error: ''
        }
    },

    props: ['image', 'name',  'urlRequest', 'urlRequestCancel'],

    computed: {
        errorArrayForAlertError: function () {
            return [this.error];
        }
    },
    components: {Errors},
    methods: {
        requestBinding() {
            this.binding = true;
            this.error = '';
            let $this = this;
            axios.post($this.urlRequest)
                .catch(errors => {
                    $this.binding = false;
                    $this.error = 'Ошибка сервера! Привязка не удалась';
                })

        },
        requestBindingCancel() {
            this.binding = false;
            this.error = '';
            let $this = this;
            axios.post($this.urlRequestCancel)
                .catch(errors => {
                    $this.binding = false;
                    $this.error = 'Ошибка сервера! Отмена привязки не удалась';
                })
        },
    }
}
