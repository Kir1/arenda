import {
    main,
    FIELD_STATUS__NO_VALIDATE,
    FIELD_STATUS__SUCCESS,
    FIELD_STATUS__ERRORS,
    REQUEST_STATUS__NOT_PUSH
} from "../main";

export const form = {
    /*
    миксин создан для поля, которое сразу является формой
    изменил и сохранил по кнопке в ней же
     */
    mixins: [main],
    methods: {
        requestField(form, fieldName, url, successFunction = null, errorFunction = null,) {
            let fieldClone = {...form[fieldName]};
            if (fieldClone.validateStatus === FIELD_STATUS__SUCCESS) {
                let data = {};
                for (let fieldName in form) {
                    fieldClone.backendErrors = [];
                    fieldClone.frontendErrors = {};
                    fieldClone.validateStatus = FIELD_STATUS__NO_VALIDATE;
                    data[fieldName] = fieldClone.value;
                }
                form[fieldName] = fieldClone;
                axios.post(url, data)
                    .then(result => {
                        if (successFunction) successFunction(result);
                    })
                    .catch(errors => {
                        if (errorFunction) errorFunction();
                        fieldClone = {...form[fieldName]};
                        if (errors.response.data.errors)
                            fieldClone.backendErrors = [...errors.response.data.errors[fieldName]];
                        else
                            fieldClone.backendErrors = [
                                `Произошла ошибка сервера! Поле не удалось сохранить. Попробуйте сделать это снова`
                            ]
                        form[fieldName] = fieldClone;
                    })
            }
        },
    },
}
