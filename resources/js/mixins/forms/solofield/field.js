import FieldContainer from "../../../components/general/form/solofield/FieldContainer";

//миксин для поля внутри контейнера поля-формы
export const field = {
    props: ['field'],
    components: {FieldContainer}
}
