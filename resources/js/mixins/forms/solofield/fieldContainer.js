import Errors from "../../../components/general/Errors";
import {FIELD_STATUS__SUCCESS, FIELD_STATUS__ERRORS, FIELD_STATUS__NO_VALIDATE} from "../main";

export const fieldContainer = {
    //миксин для контейнера поля-формы

    props: ['field', 'classesProp'],

    data() {
        return {
            edit: false
        }
    },

    components: {Errors},

    computed: {
        validateError: function () {
            return this.field.validateStatus === FIELD_STATUS__ERRORS;
        },
        validateSuccess: function () {
            return this.field.validateStatus === FIELD_STATUS__SUCCESS;
        },
        errorsValues: function () {
            return this.getFrontendErrors().concat([...this.field.backendErrors])
        },
        errorsIsset: function () {
            return this.getFrontendErrors().length > 0 || this.field.backendErrors.length > 0
        },
        classValidateStatus: function () {
            let classValidateStatus = ' field-status-validate__';
            switch (this.field.validateStatus) {
                case FIELD_STATUS__NO_VALIDATE:
                    classValidateStatus = '';
                    break;
                case FIELD_STATUS__SUCCESS:
                    classValidateStatus += 'success';
                    break;
                case FIELD_STATUS__ERRORS:
                    classValidateStatus += 'errors';
                    break;
            }
            return classValidateStatus;
        },
    },

    methods: {
        editMode() {
            this.edit = !this.edit;
        },
        getFrontendErrors() {
            //получаем только ошибки со значением, то есть существующие/активные
            return Object.values(this.field.frontendErrors).filter(function (el) {
                return el !== null;
            });
        },
        confirm() {
            this.edit = false;
            this.field.request();
        }
    },
}
