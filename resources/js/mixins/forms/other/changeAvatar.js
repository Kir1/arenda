import {getAbsoluteUrl} from "../../../helpers/general";
import {checkFileImage} from "../../../helpers/form";

export const changeAvatar = {
    methods: {
        changeAvatar(event) {
            let avatarFile = event.target.files[0];

            let $this = this;
            let avatarClone = {...$this.avatar};
            avatarClone.errors = [];
            avatarClone.file = avatarFile;

            ///errors
            if (avatarClone.file.size > 1000000) {
                avatarClone.errors.push('Файл должен весить 1мб или' +
                    ' меньше');
            }

            if (checkFileImage(avatarClone.file) == false) {
                avatarClone.errors.push('Файл не является картинкой');
            } else {
                var reader = new FileReader;
                reader.readAsDataURL(avatarClone.file);
                reader.onload = function (e) {
                    var img = new Image();
                    avatarClone.url = e.target.result;
                    img.src = e.target.result;;
                    img.onload = function () {
                        let w = img.width;
                        let h = img.height;
                        if (w > 200 || h > 200) avatarClone.errors.push('Картинка должна быть не больше размеров 200x200');
                    }
                };

            }
            /////

            $this.avatar = avatarClone;

            if (avatarClone.errors.length == 0) {
                let formData = new FormData();
                formData.append('avatar', avatarFile);
                axios.post(getAbsoluteUrl('profile/change-avatar'), formData)
                    .catch(errors => {
                        let avatarClone = {...$this.avatar};

                        if (errors.response.data.errors) avatarClone.errors.push.concat(errors.response.data.errors.avatar);
                        else avatarClone.errors = ['Произошла ошибка сервера! Перезагрузите страницу.'];

                        $this.avatar = avatarClone;
                    })
            }
        },
    },
}
