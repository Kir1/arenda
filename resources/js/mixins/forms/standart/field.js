import FieldContainer from "../../../components/general/form/standart/FieldContainer";
import {FIELD_STATUS__ERRORS, FIELD_STATUS__NO_VALIDATE, FIELD_STATUS__SUCCESS} from "../main";

export const field = {
    props: ['field'],
    computed: {
        fieldPlaceholder: function () {
            return this.field.placeholder;
        },
        classValidateStatus: function () {
            let classValidateStatus = ' field-status-validate__';
            switch (this.field.validateStatus) {
                case FIELD_STATUS__NO_VALIDATE:
                    classValidateStatus = '';
                    break;
                case FIELD_STATUS__SUCCESS:
                    classValidateStatus += 'success';
                    break;
                case FIELD_STATUS__ERRORS:
                    classValidateStatus += 'errors';
                    break;
            }
            return classValidateStatus;
        },
    },

    components: {FieldContainer}
}
