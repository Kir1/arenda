import {FIELD_STATUS__NO_VALIDATE, main, REQUEST_STATUS__NOT_PUSH, REQUEST_STATUS__PUSH} from "../main";
import {getAbsoluteUrl} from "../../../helpers/general";

export const form = {
    /*
    миксин создан для стандартной формы с полями и отправкой данных по клику на кнопку
    поля формы должны иметь поле backendErrors, в нём хранятся ошибки с бекенда
    ошибки бекенда не мешают отправке формы, отправке мешаеют только фронтенд-ошибки
    */
    mixins: [main],
    // data() {
    //     //пример итоговой стуктуры
    //     return {
    //         form: {
    //             field1: this.createFieldForm('Имя', this.validatorName)
    //validatorName принимает значение и использует методы с префиском validate
    //метод validate принимает новое значение поле, валидирует его и добавлет ошибки фронтенда в поле
    //метод validator принимает значие поля и внутри себя использует validate методы
    //         }
    //     }
    // },

    methods: {
        requestForm(formName, url, validatorForm = null, successFunction = null, errorFunction = null, fieldRequestStatus = '', preRequestAction = null) {
            if (validatorForm) validatorForm();
            let cloneForm = {...this[formName]};
            let pushAllowed = true;
            try {
                for (let fieldName in cloneForm)
                    for (let errorFieldName in cloneForm[fieldName].frontendErrors)
                        if (cloneForm[fieldName].frontendErrors[errorFieldName] !== null) throw 'break';

            } catch (e) {
                pushAllowed = false;
            }
            if (pushAllowed) {
                if (preRequestAction) preRequestAction();
                if (fieldRequestStatus) this[fieldRequestStatus] = REQUEST_STATUS__PUSH;
                let data = new FormData();
                for (let fieldName in cloneForm) {
                    cloneForm[fieldName].backendErrors = [];
                    cloneForm[fieldName].frontendErrors = {};
                    if (cloneForm[fieldName].value) data.append(fieldName, cloneForm[fieldName].value);
                }
                let $this = this;
                $this[formName] = cloneForm;
                axios.post(getAbsoluteUrl(url), data)
                    .then(result => {
                        if (fieldRequestStatus) this[fieldRequestStatus] = REQUEST_STATUS__NOT_PUSH;
                        if (successFunction) successFunction(result);
                        cloneForm = this[formName];

                    })
                    .catch(errors => {
                        if (fieldRequestStatus) this[fieldRequestStatus] = REQUEST_STATUS__NOT_PUSH;
                        cloneForm = this[formName];
                        if (errorFunction) errorFunction();
                        if (errors.response.data.errors)
                            for (let fieldName in errors.response.data.errors)
                                for (let fieldName2 in cloneForm)
                                    cloneForm[fieldName].backendErrors = [...errors.response.data.errors[fieldName]];
                        else
                            for (let fieldName in cloneForm) cloneForm[fieldName].backendErrors = ['Произошла ошибка сервера!'];
                        $this[formName] = cloneForm;
                    })
            }
        },

        validateFieldPasswordConfirm(field, valueField2) {
            this.frontendError(field, 'passwordConfirm', 'Пароли не совпадают', () => field.value !== valueField2);
        },

        createFieldForm(placeholder, validator) {
            return {
                value: '',
                placeholder,
                frontendErrors: [],
                backendErrors: [],
                validateStatus: FIELD_STATUS__NO_VALIDATE,
                validator
            }
        },

        createImageFieldForm(placeholder, validator) {
            let field = this.createFieldForm(placeholder, validator);
            field.url = getAbsoluteUrl('img/not_img.jpg');
            field.value = null;
            return field;
        },
    },
}
