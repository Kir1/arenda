import Errors from "../../../components/general/Errors";
import {FIELD_STATUS__ERRORS, FIELD_STATUS__NO_VALIDATE, FIELD_STATUS__SUCCESS} from "../main";

//миксин для контейнера полей формы, который выдаёт ошибки валидации
export const fieldContainer = {
    props: ['field', 'classesProp'],

    components: {Errors},

    computed: {
        errorsValues: function () {
            return this.getFrontendErrors().concat([...this.field.backendErrors])
        },
        errorsIsset: function () {
            return this.getFrontendErrors().length > 0 || this.field.backendErrors.length > 0
        },
        classValidateStatus: function () {
            let classValidateStatus = ' field-status-validate__';
            switch (this.field.validateStatus) {
                case FIELD_STATUS__NO_VALIDATE:
                    classValidateStatus = '';
                    break;
                case FIELD_STATUS__SUCCESS:
                    classValidateStatus += 'success';
                    break;
                case FIELD_STATUS__ERRORS:
                    classValidateStatus += 'errors';
                    break;
            }
            return classValidateStatus;
        },
    },

    methods: {
        getFrontendErrors() {
            //получаем только ошибки со значением, то есть существующие/активные
            return Object.values(this.field.frontendErrors).filter(function (el) {
                return el !== null;
            });
        },
    },
}
