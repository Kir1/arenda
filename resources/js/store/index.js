import {user} from './modules/user/index';

export const store = {
    modules: {
        user
    }
}
