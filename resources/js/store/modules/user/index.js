export const USER_INI = 'USER_INI';
export const CHANGE_USER_FIELD = 'CHANGE_USER_FIELD';
export const USER_CHANGE_REQUEST_STATUS = 'USER_CHANGE_REQUEST_STATUS';
export const USER_REQUEST_STATUS = 'USER_REQUEST_STATUS';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_DATA = 'USER_DATA';
export const USER_AUTH = 'USER_AUTH';

export const USER_CHANGE_REQUEST_STATUS_NO_PUSH = 0;
export const USER_CHANGE_REQUEST_STATUS_PUSH = 1;

export const user = {
    state: {
        user: null,
        requestStatus: USER_CHANGE_REQUEST_STATUS_PUSH
    },
    mutations: {
        [USER_INI]: (state, data) => {
            state.user = data;
        },
        [CHANGE_USER_FIELD]: (state, payload) => {
            state.user[payload.fieldName] = payload.value;
        },
        [USER_CHANGE_REQUEST_STATUS]: (state, value) => {
            state.requestStatus = value;
        },
        [USER_LOGOUT]: (state, data) => {
            state.user = null;
        }
    },
    getters: {
        [USER_DATA]: state => {
            return state.user;
        },
        [USER_REQUEST_STATUS]: state => {
            return state.requestStatus;
        },
    },
    actions: {
        [USER_AUTH]: async (context) => {
            context.commit(USER_CHANGE_REQUEST_STATUS, USER_CHANGE_REQUEST_STATUS_PUSH);
            return await axios.post('auth')
                .then(result => {
                    context.commit(USER_CHANGE_REQUEST_STATUS, USER_CHANGE_REQUEST_STATUS_NO_PUSH);
                    context.commit(USER_INI, result.data);
                    return result.data;
                })
                .catch(errors => {
                    context.commit(USER_CHANGE_REQUEST_STATUS, USER_CHANGE_REQUEST_STATUS_NO_PUSH);
                    alert('Ошибка сервера! Перезагрузите страницу.');
                })
        },
        [USER_LOGOUT]: (context, router) => {
            context.commit(USER_INI, null);
            router.push({name: 'home'});
            axios.post('logout').catch(errors => {
                alert('Ошибка сервера! Вам не удалось выйти из системы. Перезагрузите страницу.');
            })
        }
    }
}
