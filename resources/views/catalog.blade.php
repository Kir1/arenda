@extends('layout.main.layout')

@section('content')
    <div class="grey-bg my-container catalog" id="catalog-page">
        <div class="container">
            @include('other.breadcrumb')
            <div class="catalog__container">
                <div class="catalog__panel">
                    <h1 class="catalog__panel-content-title h1 text-left">Аренда квартир без посредников</h1>
                    <div class="catalog__panel-content">
                        <div class="catalog__panel-text">
                            Найдено {{ count($apartments) }} объявления:
                        </div>
                        <div class="catalog__panel-sort-filters">
                            @foreach ($sort_filters as $sort_filter)
                                @php
                                    $active = '';
                                    if ($loop->first) $active = 'active';
                                @endphp
                                <div class="catalog__panel-sort-filter {{ $active }}">{{ $sort_filter['name']  }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="catalog__main">
                    <div class="catalog__filters">
                        <div class="catalog__type-list">
                            <div class="catalog__type-list-button active">
                                <svg class="catalog__type-list-icon" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     version="1.1" x="0px" y="0px" viewBox="0 0 384 384"
                                     style="enable-background:new 0 0 384 384;" xml:space="preserve" fill="#000">
                                <g>
                                    <g>
                                        <g>
                                            <rect x="0" y="277.333" width="384" height="42.667"/>
                                            <rect x="0" y="170.667" width="384" height="42.667"/>
                                            <rect x="0" y="64" width="384" height="42.667"/>
                                        </g>
                                    </g>
                                </g>
                                </svg>

                                <div class="catalog__type-list-text">
                                    Списком
                                </div>

                            </div>
                            <div class="catalog__type-list-button catalog__type-list-button2">
                                <svg class="catalog__type-list-icon" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Слой_1" x="0px"
                                     y="0px" viewBox="0 0 214.81 283.46"
                                     style="enable-background:new 0 0 214.81 283.46;" xml:space="preserve">
                                <g>
                                    <path
                                        d="M102.21,6.16c3.65,0,7.3,0,10.95,0c0.27,0.06,0.54,0.17,0.82,0.18c6.24,0.36,12.41,1.25,18.46,2.79   c24.28,6.15,43.96,19.28,58.9,39.35c11.62,15.6,18.21,33.19,19.95,52.58c0.11,1.22,0.27,2.43,0.4,3.65c0,3.65,0,7.3,0,10.95   c-0.06,0.28-0.15,0.55-0.18,0.83c-0.44,3.84-0.69,7.71-1.34,11.51c-3.86,22.23-13.97,41.19-30.11,56.92   c-10.81,10.54-23.38,18.3-37.59,23.39c-0.72,0.26-1.02,0.7-1.27,1.34c-2.99,7.51-5.99,15.02-9,22.52   c-5.18,12.95-10.36,25.9-15.53,38.86c-0.96,2.41-2.55,4.24-4.89,5.36c-4.86,2.32-10.96,0.19-13.28-5.68   c-8.04-20.36-16.22-40.66-24.32-60.99c-0.31-0.79-0.75-1.19-1.55-1.48c-15.26-5.54-28.61-14.08-39.84-25.82   C15.89,164.74,6.25,143.7,4.07,119.3C2.8,105.05,4.3,91.08,8.94,77.56c9.6-28.03,27.85-48.52,54.47-61.48   c11.12-5.41,22.93-8.49,35.26-9.52C99.85,6.46,101.03,6.29,102.21,6.16z M107.6,147.9c20.69,0.01,37.69-16.86,37.76-37.37   c0.07-20.8-16.52-37.52-36.83-37.97c-21.09-0.46-38.43,16.51-38.5,37.48C69.96,130.93,86.85,147.89,107.6,147.9z"/>
                                </g>
                                </svg>
                                <div class="catalog__type-list-text">
                                    На карте
                                </div>
                            </div>
                        </div>
                        <div class="catalog__filters-blocks">
                            <div class="catalog__filters-block catalog__filters-block1">
                                <div class="catalog__filters-apartments-and-rooms">
                                    <div class="catalog__filters-button-apartments">Квартиры</div>
                                    <div class="catalog__filters-button-rooms">Комнаты</div>
                                </div>
                                <div class="catalog__filters-station-and-scheme">
                                    <input class="catalog__filters-station" placeholder="Введите название станции"/>
                                    <div class="catalog__filters-scheme">
                                        <svg class="catalog__filters-scheme-icon" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="37px" height="37px"
                                             viewBox="0 0 37 37" version="1.1">
                                            <!-- Generator: sketchtool 3.8.3 (29802) - http://www.bohemiancoding.com/sketch -->
                                            <title>57E9CD32-036A-4857-ABFB-B63E14678FD4</title>
                                            <desc>Created with sketchtool.</desc>
                                            <defs>
                                                <circle id="path-1" cx="16.5" cy="16.5" r="12.5"/>
                                                <mask id="mask-2" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="25" height="25"
                                                      fill="white">
                                                    <use xlink:href="#path-1"/>
                                                </mask>
                                                <circle id="path-3" cx="13" cy="14" r="3"/>
                                                <mask id="mask-4" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-3"/>
                                                </mask>
                                                <path
                                                    d="M24,28 C25.6568542,28 27,26.6568542 27,25 C27,23.3431458 25.6568542,22 24,22 C22.3431458,22 21,23.3431458 21,25 C21,26.6568542 22.3431458,28 24,28 Z"
                                                    id="path-5"/>
                                                <mask id="mask-6" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-5"/>
                                                </mask>
                                                <path
                                                    d="M8,12 C9.65685425,12 11,10.6568542 11,9 C11,7.34314575 9.65685425,6 8,6 C6.34314575,6 5,7.34314575 5,9 C5,10.6568542 6.34314575,12 8,12 Z"
                                                    id="path-7"/>
                                                <mask id="mask-8" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-7"/>
                                                </mask>
                                                <path
                                                    d="M19,23 C20.6568542,23 22,21.6568542 22,20 C22,18.3431458 20.6568542,17 19,17 C17.3431458,17 16,18.3431458 16,20 C16,21.6568542 17.3431458,23 19,23 Z"
                                                    id="path-9"/>
                                                <mask id="mask-10" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-9"/>
                                                </mask>
                                                <circle id="path-11" cx="21" cy="6" r="3"/>
                                                <mask id="mask-12" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-11"/>
                                                </mask>
                                                <circle id="path-13" cx="12" cy="27" r="3"/>
                                                <mask id="mask-14" maskContentUnits="userSpaceOnUse"
                                                      maskUnits="objectBoundingBox" x="0" y="0" width="6" height="6"
                                                      fill="white">
                                                    <use xlink:href="#path-13"/>
                                                </mask>
                                            </defs>
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                                <g id="мини-список-десктоп"
                                                   transform="translate(-276.000000, -369.000000)">
                                                    <g id="Group-3" transform="translate(60.000000, 133.000000)">
                                                        <g id="scheme-metro"
                                                           transform="translate(218.000000, 238.000000)">
                                                            <path d="M-8.01136935e-13,27 L27,3.96572604e-13" id="Line"
                                                                  stroke="#ED1C35" stroke-width="2"
                                                                  stroke-linecap="square"/>
                                                            <path d="M6,33 L33,6" id="Line-Copy-28" stroke="#F58132"
                                                                  stroke-width="2" stroke-linecap="square"/>
                                                            <use id="Oval-522" stroke="#874E33" mask="url(#mask-2)"
                                                                 stroke-width="4" xlink:href="#path-1"/>
                                                            <path d="M2,3 L29,30" id="Line-Copy-25" stroke="#18A24A"
                                                                  stroke-width="2" stroke-linecap="square"/>
                                                            <use id="Oval-516" stroke="#ED1C35" mask="url(#mask-4)"
                                                                 stroke-width="4" fill="#FFFFFF" xlink:href="#path-3"/>
                                                            <use id="Oval-516-Copy" stroke="#18A24A" mask="url(#mask-6)"
                                                                 stroke-width="4" fill="#FFFFFF" xlink:href="#path-5"/>
                                                            <use id="Oval-516-Copy" stroke="#18A24A" mask="url(#mask-8)"
                                                                 stroke-width="4" fill="#FFFFFF" xlink:href="#path-7"/>
                                                            <use id="Oval-516-Copy" stroke="#F58132"
                                                                 mask="url(#mask-10)" stroke-width="4" fill="#FFFFFF"
                                                                 xlink:href="#path-9"/>
                                                            <use id="Oval-516-Copy-2" stroke="#ED1C35"
                                                                 mask="url(#mask-12)" stroke-width="4" fill="#FFFFFF"
                                                                 xlink:href="#path-11"/>
                                                            <use id="Oval-516-Copy-2" stroke="#F58132"
                                                                 mask="url(#mask-14)" stroke-width="4" fill="#FFFFFF"
                                                                 xlink:href="#path-13"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <div class="catalog__filters-scheme-text">Выбрать на схеме</div>
                                    </div>
                                </div>

                                <div class="catalog__filters-distance-and-select">
                                    <div class="catalog__filters-title">Расстояние от метро</div>
                                    <div class="pseudo-select">Не важно</div>
                                </div>

                                <div class="catalog__filters-title catalog__filters-title2">Цена</div>
                                <div class="catalog__filters-range-two-input">
                                    @include('other.range-two-input', ['min' => 0, 'max' =>'25.000.000'])
                                </div>

                                <div class="catalog__filters-title">Источник</div>
                                <div class="catalog__filters-source container-two-line-blocks">
                                    <div class="container-two-line-block container-two-line-block1">
                                        <div class="container-two-line-blocks-title">
                                            Только проверенные
                                        </div>
                                        @include('other.info-circle', ['title'=>"Только из проверенных источников, одобренных модераторами"])
                                    </div>
                                    <div class="container-two-line-block container-two-line-block2">
                                        @include('other.switcher')
                                    </div>
                                </div>

                                <div class="catalog__filters-title">Объявления с комиссией</div>
                                <div class="catalog__filters-source container-two-line-blocks">
                                    <div class="container-two-line-block container-two-line-block1">
                                        <div class="container-two-line-blocks-title">
                                            Показывать
                                        </div>
                                    </div>
                                    <div class="container-two-line-block container-two-line-block2">
                                        @include('other.switcher')
                                    </div>
                                </div>

                                <div class="catalog__filters-title">Коммиссия до</div>
                                <div class="catalog__filters-range-percent">
                                    @include('other.range-percent')
                                </div>
                            </div>

                            <div class="catalog__filters-block">
                                <div class="catalog__filters-additionally container-two-line-blocks">
                                    <div class="container-two-line-block container-two-line-block1">
                                        <div class="container-two-line-blocks-title">
                                            Дополнительно
                                        </div>
                                    </div>
                                    <div class="container-two-line-block container-two-line-block2">
                                        <div class="down-triangle"></div>
                                    </div>
                                </div>

                                <div class="catalog__filters-area-and-floor">
                                    <div class="catalog__filters-area-and-floor-block catalog__filters-area">
                                        <div class="catalog__filters-area-and-floor-block-title catalog__filters-title">
                                            Площадь в м²
                                        </div>
                                        <div class="catalog__filters-area-and-floor-block-inputs">
                                            <input class="catalog__filters-area-and-floor-block-input"/>
                                            <div class="catalog__filters-area-and-floor-block-hyphen">-</div>
                                            <input class="catalog__filters-area-and-floor-block-input"/>
                                        </div>
                                    </div>
                                    <div class="catalog__filters-area-and-floor-block catalog__filters-floor">
                                        <div class="catalog__filters-area-and-floor-block-title catalog__filters-title">
                                            Этаж
                                        </div>
                                        <div class="catalog__filters-area-and-floor-block-inputs">
                                            <input class="catalog__filters-area-and-floor-block-input"/>
                                            <div class="catalog__filters-area-and-floor-block-hyphen">-</div>
                                            <input class="catalog__filters-area-and-floor-block-input"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__filters-amenities">
                                    <div class="catalog__filters-checkbox-block">
                                        <div class="catalog__filters-checkbox-block-title catalog__filters-title">
                                            Удобства
                                        </div>
                                        <div class="catalog__filters-checkbox-block-list">
                                            @php
                                                $listForCheckbox = [
                                                    'Балкон',
                                                    'Посудомоечная машина',
                                                    'Холодильник ',
                                                    'Стиральная машина',
                                                    'Телевизор',
                                                    'Нагреватель воды ',
                                                    'Концеционер',
                                                ];
                                            @endphp
                                            @foreach ($listForCheckbox as $listValue)
                                                <div class="catalog__filters-checkbox">
                                                    <div class="catalog__filters-checkbox-value">
                                                        <i class="check fas fa-check"></i>
                                                    </div>
                                                    <div class="catalog__filters-checkbox-text">
                                                        {{  $listValue }}
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="catalog__filters-features">
                                    <div class="catalog__filters-checkbox-block">
                                        <div class="catalog__filters-checkbox-block-title catalog__filters-title">
                                            Особенности
                                        </div>
                                        <div class="catalog__filters-checkbox-block-list">
                                            @php
                                                $listForCheckbox = [
                                                    'Можно курить',
                                                    'Подходит для мероприятий',
                                                    'Можно с животными',
                                                    'Подходиит для семьи с детьми',
                                                ];
                                            @endphp
                                            @foreach ($listForCheckbox as $listValue)
                                                <div class="catalog__filters-checkbox">
                                                    <div class="catalog__filters-checkbox-value">
                                                        <i class="check fas fa-check"></i>
                                                    </div>
                                                    <div class="catalog__filters-checkbox-text">
                                                        {{  $listValue }}
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="catalog__apartments">
                        @if (count($apartments) > 1)
                            @include('catalog.catalog', ['apartments'=>$apartments])
                        @else
                            Квартир не найдено.
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <svg class="catalog-mobile-filters-button" viewBox="0 0 25 19" id="icon-filter">
            <path
                d="M4 4a4 4 0 0 1 .14-1H0v2h4.14A4 4 0 0 1 4 4zM11.86 3A4 4 0 0 1 12 4a4 4 0 0 1-.14 1H25V3zM13 15a4 4 0 0 1 .14-1H0v2h13.14a4 4 0 0 1-.14-1zM20.86 14a3.61 3.61 0 0 1 0 2H25v-2z"></path>
            <path
                d="M8 2a2 2 0 1 1-2 2 2 2 0 0 1 2-2m0-2a4 4 0 1 0 4 4 4 4 0 0 0-4-4zM17 13a2 2 0 1 1-2 2 2 2 0 0 1 2-2m0-2a4 4 0 1 0 4 4 4 4 0 0 0-4-4z"></path>
        </svg>
    </div>
@endsection
