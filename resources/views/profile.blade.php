@extends('layout.main.layout')

@section('content')
    <div class="grey-bg my-container">
        <div class="profile-panel">
            <div class="container">
                <div class="profile-panel__container">
                    <div class="profile-panel__left">
                        <a href="{{ route('home') }}">Мои объявления</a>
                        <a href="{{ route('home') }}">Избранное</a>
                        <a href="{{ route('home') }}">Мои подписки</a>
                        <a href="{{ route('home') }}">Уведомления</a>
                    </div>
                    <div class="profile-panel__right">
                        <a class="link-button" href="{{ route('home') }}">Личные данные</a>
                        <a href="{{ route('logout-page') }}">Выход</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @include('other.breadcrumb')
            <page-profile></page-profile>
        </div>
    </div>
@endsection
