<div class="range-percent">
    <div class="range-percent__switcher">
        <div class="range-percent__line"></div>
        <div class="range-percent__circle range-percent__circle"></div>
    </div>
    <div class="range-percent__value">100%</div>
</div>
