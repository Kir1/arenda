<div class="range-two-input">
    <div class="range-two-input__switcher">
        <div class="range-two-input__line"></div>
        <div class="range-two-input__circle range-two-input__circle1"></div>
        <div class="range-two-input__circle range-two-input__circle2"></div>
    </div>
    <div class="range-two-input__fields">
        <div class="range-two-input__field">
            <div class="range-two-input__text">
                от
            </div>
            <input class="range-two-input__value" placeholder="{{ $min }}"/>
        </div>
        <div class="range-two-input__field">
            <div class="range-two-input__text">
                до
            </div>
            <input class="range-two-input__value" placeholder="{{ $max }}"/>
        </div>
    </div>
</div>
