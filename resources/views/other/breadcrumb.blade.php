<div class="crumbs">
    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        @foreach ($breadcrumbs ?? '' as $link)
            @if ($loop->last)
                <li class="crumb active">{{$link['text']}}</li>
            @else
                <li class="crumb" itemprop="itemListElement" itemscope
                    itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="{{$link['url']}}">
                        <span itemprop="name">{{$link['text']}}</span></a>
                    <span class="crumb-item" itemprop="position" content="{{ $loop->index }}"></span>
                </li>
            @endif
        @endforeach
    </ol>
</div>
