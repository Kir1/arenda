<header>
    <div class="container">
        <a href="{{ url('/') }}" class="logo" alr = "Keymap">
            KEYMAP
        </a>
        <div class="wrap-nav">
            <button class="close-nav"></button>
            <nav>
                <ul class="menu">
                    <li><a href="{{ route('catalog') }}" title="Найти квартиру">Найти квартиру</a></li>
                    <li><a href="">На карте</a></li>
                    <li><a href=""><span>Личный помощник</span></a></li>
                </ul>
            </nav>
        </div>
        <div class="header-content">
            <div class="change-city change-city-header">
                <div class="change-city-select-text">{{$cities[0]['name']}}</div>
                <select class="change-city-select change-city-select-header" id = "change-city-select-header">
                    @foreach ($cities as $city)
                        <option value="{{$city['domain']}}">{{$city['name']}}</option>
                    @endforeach
                </select>
                <i  class="change-city-select-header__arrow fas fa-location-arrow"></i>
            </div>
            <a href="{{ route('create-room-get') }}" class="header-btn rent-property-pc">Сдать недвижимость</a>
            <div class="header-user" id="header-user">
                <div class="header-content-pc">
                    @if ($user)
                        @if ($user['countDaysEndSubscription'] > 0)
                        <span class="header-number">{{ $user['countDaysEndSubscription'] }}</span>
                        @endif
                        <div class="dropdown">
                            <button class="user-select">
                                <img src="{{ $user['avatar'] }}" alt="{{ $user['name'] }}">
                            </button>
                        </div>
                    @else
                        <a class="login-link" href="{{ route('login-page') }}" title="Войти">Войти</a>
                    @endif
                </div>
                <button class="open-nav"></button>
            </div>
        </div>
    </div>
</header>
