<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layout.main.head')
<body>
<div id="app">
    <div class="wrapper" id="main-json" data-json="{{$mainJson}}">
        <main>
            @yield('content')
        </main>
    </div>
</div>
</body>
</html>
