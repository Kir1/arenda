<footer>
    <div class="container">
        <div class="footer__wrap">
            <div class="footer-item">
                <div class="footer-title">Keymap</div>
                <ul class="footer-list">
                    <li><span></span><a href="" class="footer-helper">Личный помощник</a></li>
                    <li><span></span><a href="{{ route('terms-of-use') }}">Пользовательское соглашение</a></li>
                    <li><span></span><a href="{{ route('personal-data-processing') }}">Обработка персональных данных</a></li>
                </ul>
            </div>
            <div class="footer-item">
                <div class="footer-title">Арендаторам</div>
                <ul class="footer-list">
                    <li><span></span><a href="{{ route('catalog') }}" title="Найти квартиру">Найти квартиру</a></li>
                    <li><span></span><a href="{{ route('faq-for-tenants') }}" title="Частые вопросы">Частые вопросы</a></li>
                    <li><span></span><a href="{{ route('rental-guide') }}">Полный гайд по аренде</a></li>
                    <li><span></span><a href="">Страхование имущества</a></li>
                </ul>
            </div>
            <div class="footer-item">
                <div class="footer-title">Владельцам</div>
                <ul class="footer-list">
                    <li><span></span><a href="">Договор аренды</a></li>
                    <li><span></span><a href="{{ route('faq-for-owners') }}">Частые вопросы</a></li>
                    <li><span></span><a href="">Страхование недвижимости</a></li>
                    <li><span></span><a href="{{ route('rent-apartment') }}">Сдать квартиру</a></li>
                    <li><span></span><a href="{{ route('rent-room') }}">Сдать комнату</a></li>
                </ul>
            </div>
            <div class="footer-item">
                <div class="footer-title">Мы на связи</div>
                <span><a href="mailto:info@keymap.ru" class="foot-mail">info@keymap.ru</a></span>
            </div>

        </div>
        <div class="footer__lower-wrap">
            <span>©2016-2020 KEYMAP. Использование материалов KEYMAP возможно только при наличии активной ссылки на первоисточник.</span>
            <span>Использование сайта, в том числе подача объявлений, означает согласие с <a
                    href="{{ route('terms-of-use') }}" title="польозвательским соглашением">пользовательским соглашением.</a></span>
        </div>
    </div>
</footer>
