<div class="offer__wrap">
    @foreach ($apartments as $apartment)
        <div class="offer-item">
            <div class="offer-swiper">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach ($apartment['images'] as $imageUrl)
                            <div class="swiper-slide">
                                <img src="{{ $imageUrl }}" alt="{{ $apartment['name'] }} - фото 1" title="{{ $apartment['name'] }} - фото 1">
                            </div>
                        @endforeach
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <div class="pagination-num">
                        <span>1</span>/<span>{{ count($apartment['images']) }}</span>
                    </div>
                </div>
                @if ($apartment['status_id'] === \App\Services\AppartamentsService::APPARTAMENT_STATUS_ID__POPULATED)
                    <span class="round-color blue tooltip" title="Планируется  освобождение с {{ $apartment['date_of_eviction'] }}"></span>
                @elseif ($apartment['status_id'] === \App\Services\AppartamentsService::APPARTAMENT_STATUS_ID__CHECKED_BY_MODERATOR)
                    <span class="round-color green tooltip" title="Объявление проверено модератором."></span>
                @elseif ($apartment['status_id'] === \App\Services\AppartamentsService::APPARTAMENT_STATUS_ID__CHECKED_BY_ROBOT)
                    <span class="round-color yellow tooltip" title="Объявление проверено искусственным интеллектом. "></span>
                @endif
            </div>
            <div class="swiper-text">
                <div class="swiper-desc">
                    <p>{{ $apartment['number_of_rooms'] }}-к квартира</p>
                    <p>{{ $apartment['area'] }}м2</p>
                    <p>этаж {{ $apartment['floor'] }}</p>
                </div>
                <div class="swiper-location">
                    <p>{{ $apartment['name'] }}</p>
                </div>
                <span>35 000 ₽/мес</span>
                <div class="swiper-text__media">
                    <a href="" class="swiper-heart"  title="добавить в избранное"></a>
                    <a href="" class="swiper-upload">
                        <img src="{{ url('img/upload.png') }}" alt="скачать" title="скачать">
                    </a>
                </div>
            </div>
        </div>
    @endforeach
</div>
