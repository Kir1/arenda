@extends('layout.main.layout')

@section('content')
    <div class="wrapper">
        <div class="home">
            <div class="container" style="visibility: visible;">
                <a href="{{ route('home') }}" class="return-btn">Вернуться на главную</a>
                <div class="logo_img text-center">
                    <img src="{{ url('img/logo_white.png') }} " alt="Keymap" title="Keymap">
                </div>
                <h1 class="rent-title text-center rent-title-h1"><b>Хотите сдать квартиру?</b><br>Наш сайт поможет!</h1>
                <div class="rent-btn__wrap">
                    <a href="{{ route('place-an-ad') }}" title="Разместить объявление" class="yellow-btn">Разместить объявление</a>
                </div>
                <span class="text-center">Узнать преимущества</span>
            </div>
        </div>
        <div class="advantages">
            <div class="container-fluid">
                <h2 class="our-advantages">Наши преимущества</h2>
                <div class="advanteges__wrap">
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/1.png" alt="">
                        </div>
                        <span class="advantages-title">Бесплатно</span>
                        <p>Просто размещайте объявление и ждите звонков от арендаторов</p>
                    </div>
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/2.png" alt="">
                        </div>
                        <span class="advantages-title">Без риэлторов</span>
                        <p>Мы составили для вас продуманный договор — скачивайте его и подписывайте с арендаторами</p>
                    </div>
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/3.png" alt="">
                        </div>
                        <span class="advantages-title">Широкая аудитория</span>
                        <p>Каждый день сайт посещают более 8000 человек</p>
                    </div>
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/4.png" alt="">
                        </div>
                        <span class="advantages-title">Приличные люди</span>
                        <p>Наши посетители — приличные, образованные, платежеспособные люди</p>
                    </div>
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/5.png" alt="">
                        </div>
                        <span class="advantages-title">Служба поддержки</span>
                        <p>Наш сайт устроен очень просто, однако если у вас появятся вопросы, вы можете писать на почту info@keymap.ru</p>
                    </div>
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="img/advantages/6.png" alt="">
                        </div>
                        <span class="advantages-title">Безопасно</span>
                        <p>Ваши контакты будут видны только платным подписчикам</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="how-it-works">
            <div class="container-fluid">
                <h2 class="how-it-work">Как это работает?</h2>
                <div class="how__wrap">
                    <div class="how-item">
                        <div class="how-img">
                            <img src="img/how-it-works/1.png" alt="">
                        </div>
                        <p>Вы сами размещаете свое объявление на сайте</p>
                    </div>
                    <div class="how-item">
                        <div class="how-img">
                            <img src="img/how-it-works/2.png" alt="">
                        </div>
                        <p>Объявление проходит модерацию и попадает на сайт</p>
                    </div>
                    <div class="how-item">
                        <div class="how-img">
                            <img src="img/how-it-works/3.png" alt="">
                        </div>
                        <p>Вам начинают приходить заявки от арендаторов</p>
                    </div>
                </div>
                <div class="how-btn">
                    <a href="{{ route('place-an-ad') }}" title="Разместить объявление о сдаче квартиры" class="yellow-btn">Разместить объявление о сдаче квартирые</a>
                </div>
            </div>
        </div>
        <div class="about-us">
            <div class="container">
                <div class="about-us__title">
                    <h2>Отзывы</h2>
                    <div class="swiper-arrows">
                        <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide"></div>
                        <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide"></div>
                    </div>
                </div>
                <div class="about-us__swiper" style="visibility: visible;">
                    <div class="swiper_second-container swiper-container-initialized swiper-container-horizontal swiper-container-css-mode">
                        <div class="swiper-wrapper"><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider3.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Растислав Сергеевич</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(5,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Поиски квартиры заняли ровно неделю, Андрей проверял все варианты и составлял подробное описание. Так же оказал помощь при подписании договора. И вот теперь, я снимаю квартиру без комиссии, от собственника, с хорошим ремонтом, в новом доме по приемлемой цене.
                                    </div>
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider1.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Александр Галка</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска требования.

                                    </div>
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="4" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider2.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Валерия Александровна</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения. Документы с арендодателем я подписала в конце той же недели и сразу переехала. Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком. Просто красота!
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider1.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Александр Галка</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска требования.

                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider2.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Валерия Александровна</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения. Документы с арендодателем я подписала в конце той же недели и сразу переехала. Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком. Просто красота!
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider3.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Растислав Сергеевич</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(5,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Поиски квартиры заняли ровно неделю, Андрей проверял все варианты и составлял подробное описание. Так же оказал помощь при подписании договора. И вот теперь, я снимаю квартиру без комиссии, от собственника, с хорошим ремонтом, в новом доме по приемлемой цене.
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="3" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider1.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Александр Галка</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска требования.

                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="4" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider2.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Валерия Александровна</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения. Документы с арендодателем я подписала в конце той же недели и сразу переехала. Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком. Просто красота!
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider1.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Александр Галка</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска требования.

                                    </div>
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" style="width: 368px; margin-right: 38px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider2.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Валерия Александровна</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(4,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения. Документы с арендодателем я подписала в конце той же недели и сразу переехала. Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком. Просто красота!
                                    </div>
                                </div>
                            </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="width: 368px;">
                                <div class="swiper_second-slide">
                                    <div class="swiper-slider__user">
                                        <div class="help-right__user">
                                            <img src="img/slider3.png" alt="">
                                            <span></span>
                                        </div>
                                        <div class="swiper-slider_name">
                                            <span>Растислав Сергеевич</span>
                                            <div class="rating">
                                                <div class="article__interaction-point-stars">
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                    <a href="#" class="active choosed">
                                                        <svg enable-background="new 0 0 511.998 511.998" height="512" viewBox="0 0 511.998 511.998" width="512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z" fill="#ababab"></path></g></g></svg>
                                                    </a>
                                                </div>
                                                <span>(5,0)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slider__text">
                                        Поиски квартиры заняли ровно неделю, Андрей проверял все варианты и составлял подробное описание. Так же оказал помощь при подписании договора. И вот теперь, я снимаю квартиру без комиссии, от собственника, с хорошим ремонтом, в новом доме по приемлемой цене.
                                    </div>
                                </div>
                            </div></div>
                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span></div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                </div>
            </div>
        </div>
    </div>
@endsection
