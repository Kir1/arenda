@extends('layout.main.layout')

@section('content')
    <div class="grey-bg my-container">
        <div class="container">
            @include('other.breadcrumb')
            <h1 class="h1">{{ $title }}</h1>
            <page-create-update-room/>
        </div>
    </div>
@endsection
