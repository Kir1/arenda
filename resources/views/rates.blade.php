@extends('layout.main.layout')

@section('content')
    <div class="grey-bg my-container rates">
        <div class="container">
            @include('other.breadcrumb')
            <h1 class="h1">Квартира на Никитинской улице</h1>
            <p class="advertise-your-ad">Рекламируйте ваше объявление и повышайте количество просмотров<p>
            <div class="rates__list">
                @foreach ($rates as $rate)
                    @php
                        $popularClass = '';
                        if (isset($rate['popular']))  $popularClass = 'popular';
                    @endphp
                    <div class="rate {{ $popularClass }}">
                        @if ($popularClass)
                            <img class="popular-img" src="{{ url('img/popular.png') }}" alt="Популярный" title="Популярный">
                        @endif
                        <div class="rate__block-child1">
                            <div class="rate__title">
                                {{ $rate['name'] }}
                            </div>
                            <div class="rate__days-and-triangle">
                                <div class="rate__days">
                                    {{ $rate['days'] }} дня
                                </div>
                                <div class="rate__triangle"></div>
                            </div>
                        </div>
                        <div class="rate__block-child2">
                            @foreach ($rate['features'] as $feature)
                                @php
                                    $disableClass = '';
                                    if (isset($feature['check']) && $feature['check'] == false)  $disableClass = 'disable';
                                @endphp
                                <div class="rate__feature {{ $disableClass }}">
                                    <div class="rate__feature-icon">
                                        @if ($feature['check'])
                                            <i class="fas fa-check"></i>
                                        @else
                                            <i class="fas fa-times"></i>
                                        @endif
                                    </div>
                                    <div class="rate__feature-text">
                                        {!! $feature['text']  !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="rate__block-child3">
                            <div class="rate__price">
                                {{ $rate['price'] }} ₽
                            </div>
                            <div class="rate__choose giant-button">Выбрать</div>
                            <div class="rate__choosen">
                                <div class="rate__choosen-icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="rate__choosen-text">
                                    Выбрано
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="rate__buttons">
                <div id="rate-no-commerce-button" class="rate__button">
                    Не рекламировать
                </div>
                <div id="rate-pay-button" class="rate__button">
                    Оплатить
                </div>
            </div>
        </div>
    </div>
@endsection
