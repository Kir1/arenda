@extends('layout.main.layout')

@section('content')
    <div class="welcome">
        <div class="container">
            <div class="welcome__wrap">
                <h1>
                    <div>Найдем ваше идеальное</div>

                    <div class="find-you-ideal-house">
                        <div class="house-in-city-text">жилье в городе</div>
                        <div class="change-city index">

                            <div class="change-city-select-text">{{$cities[0]['name']}}</div>

                            <select class="change-city-select">
                                @foreach ($cities as $city)
                                    <option value="{{$city['id']}}">{{$city['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </h1>
                <div class="welcome__wrap-filter {{ isset($mertoList)? 'welcome__wrap-with-metro' : ''}}">
                    @if (isset($mertoList))
                        <div class="metro">
                            <input type="text" placeholder="Метро">
                        </div>
                    @endif
                    <div class="filter">
                        <span>Комната</span>
                        <div class="checkbox__wrap">
                            <input type="checkbox" name="choice" id="cb1"/><label class="tooltip" for="cb1"
                                                                                  title="однокомнатные квартиры">1</label>
                            <input type="checkbox" name="choice" id="cb2"/><label class="tooltip" for="cb2"
                                                                                  title="двухкомнатные квартиры">2</label>
                            <input type="checkbox" name="choice" id="cb3"/><label class="tooltip" for="cb3"
                                                                                  title="трехкомнатные квартиры">3</label>
                            <input type="checkbox" name="choice" id="cb4"/><label class="tooltip" for="cb4"
                                                                                  title="многокомнатные квартиры">4+</label>
                        </div>
                    </div>
                    <div class="location">
                        <input type="text" placeholder="Район, жилой комплекс, микрорайон, улица">
                    </div>
                    <button type="submit">Найти объявления</button>
                </div>
            </div>

            <div class="banner">
                <div id="soc-slider" class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="soc-slider-banner">
                                <img class="soc-slider-banner__icon" width="40" height="40"
                                     src="{{ url('img/icon-yandex-approves.png') }}">
                                <p class="title-yandex">Выбор пользователей <br> Яндекса</p>
                            </div>
                        </div>
                        @foreach ($socnetServices as $socnetService)
                            <div class="swiper-slide">
                                <div class="soc-slider-banner">
                                    <img class="soc-slider-banner__icon" width="40" height="40"
                                         src="{{ $socnetService['img'] }}">
                                    <div class="soc-slider-banner__media">
                                        <div class="row1">
                                            <p class="rating">{{ $socnetService['rating'] }}</p>
                                            <div class="stars">
                                                <?php
                                                $starsCount = ceil($socnetService['rating']);
                                                ?>
                                                @for ($i = $starsCount; $i > 0; $i--)
                                                    <?php
                                                    //last
                                                    if ($i == 1) {
                                                        $starFullnessIndex = $socnetService['rating'];
                                                        if (is_int($socnetService['rating']) == false) {
                                                            $starFullnessIndex = (string)$socnetService['rating'];
                                                            $starFullnessIndex = explode(".", $starFullnessIndex)[1]; //1 to 9
                                                        } else $starFullnessIndex = 10;
                                                    } else $starFullnessIndex = 10;
                                                    $starFullnessClass = 'star-fullness' . $starFullnessIndex;
                                                    ?>
                                                    <div class="star {{ $starFullnessClass }}"></div>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <p class="title">{{ $socnetService['name'] }}</p>
                                            <p class="count-of-reviews">{{ $socnetService['count_of_reviews'] }}
                                                отзыв</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>

            <ul class="banner-mobile">
                @foreach ($socnetServices as $socnetService)
                    <li class="banner-mobile__item">
                        <img class="banner-mobile__img" width="40" height="40" src="{{ $socnetService['img'] }}">
                        <div class="banner-mobile__rating">{{ $socnetService['rating'] }}</div>
                        <div class="banner-mobile__stars stars">
                            <div class="star star-fullness10"></div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="benefits">
        <div class="container">
            <h2>Наши преимущества</h2>
            <div class="benefits__wrap">
                <div class="benefits-item">
                    <span class="benefits-num">01</span>
                    <div class="benefits-img">
                        <img src="img/ben1.png" alt="">
                    </div>
                    <span class="benefits-title">Квартиры от собственников</span>
                    <p>Квартиры, отмеченные зеленым кружком, сдаются напрямую от собственников. Модератор проверяет
                        каждое объявление, чтобы убедиться, что посредников и комиссии действительно нет.</p>
                </div>
                <div class="benefits-item">
                    <span class="benefits-num">02</span>
                    <div class="benefits-img">
                        <img src="img/ben2.png" alt="">
                    </div>
                    <span class="benefits-title">Только актуальные
объявления</span>
                    <p>Специальный алгоритм удаляет дубли, проверяет актуальность объявлений и вычисляет соответствие
                        цены площади и местоположению квартиры.</p>
                </div>
                <div class="benefits-item">
                    <span class="benefits-num">03</span>
                    <div class="benefits-img">
                        <img src="img/ben3.png" alt="">
                    </div>
                    <span class="benefits-title">Сделаем всё за вас</span>
                    <p>Найдем квартиру мечты, составим выгодный договор и сэкономим вам силы и время. Личный помощник
                        возьмет все заботы на себя!</p>
                </div>
                <div class="benefits-item">
                    <span class="benefits-num">04</span>
                    <div class="benefits-img">
                        <img src="img/ben4.png" alt="">
                    </div>
                    <span class="benefits-title">Самая полная база</span>
                    <p>Мониторим все соцсети и площадки по аренде недвижимости — вы не пропустите свой вариант.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="help">
        <div class="container">
            <div class="help__wrap">
                <div class="help__wrap-left">
                    <h2>Личный помощник поможет найти для вас идеальное жилье</h2>
                    <div class="help-left__content">
                        <div class="help-buttons__wrap">
                            <div class="help-button__item">
                                <button type="button" class="help-btn active">
                                    <img src="img/svg/help1.svg" alt="">
                                </button>
                            </div>
                            <div class="help-button__item">
                                <button type="button" class="help-btn">
                                    <img src="img/svg/help2.svg" alt="">
                                </button>
                            </div>
                            <div class="help-button__item">
                                <button type="button" class="help-btn">
                                    <img src="img/svg/help3.svg" alt="">
                                </button>
                            </div>
                        </div>
                        <div class="help-buttons__text active">
                            <span>Экономия</span>
                            <p>Личный помощник стоит </p>
                            <p>гораздо дешевле риелтора.</p>
                        </div>
                        <div class="help-buttons__text">
                            <span>2222</span>
                            <p>Личный помощник стоит </p>
                            <p>гораздо дешевле риелтора.</p>
                        </div>
                        <div class="help-buttons__text">
                            <span>3333</span>
                            <p>Личный помощник стоит </p>
                            <p>гораздо дешевле риелтора.</p>
                        </div>
                    </div>
                </div>
                <div class="help-right">
                    <div class="help__wrap-right active">
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>Здравствуйте!</p>
                                <p>Наткнулась на ваш сайт. Подскажите, сколько стоят ваши услуги? Вы что-то типа
                                    риелторов?</p>
                            </div>
                        </div>
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user2.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>Добрый день!</p>
                                <p>Нет, мы не риелторы :)</p>
                                <p>Мы помогаем быстро и эффективно находить жилье и у нас нет комиссий.</p>
                                <p>Цена фиксированная и не зависит от стоимости квартиры.</p>
                            </div>
                        </div>
                    </div>
                    <div class="help__wrap-right">
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>22222!</p>
                                <p>Наткнулась на ваш сайт. Подскажите, сколько стоят ваши услуги? Вы что-то типа
                                    риелторов?</p>
                            </div>
                        </div>
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user2.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>2222222</p>
                                <p>Нет, мы не риелторы :)</p>
                                <p>Мы помогаем быстро и эффективно находить жилье и у нас нет комиссий.</p>
                                <p>Цена фиксированная и не зависит от стоимости квартиры.</p>
                            </div>
                        </div>
                    </div>
                    <div class="help__wrap-right">
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>333333</p>
                                <p>Наткнулась на ваш сайт. Подскажите, сколько стоят ваши услуги? Вы что-то типа
                                    риелторов?</p>
                            </div>
                        </div>
                        <div class="help-right__item">
                            <div class="help-right__user">
                                <img src="img/help_user2.png" alt="">
                                <span></span>
                            </div>
                            <div class="help-right__text">
                                <p>3333333</p>
                                <p>Нет, мы не риелторы :)</p>
                                <p>Мы помогаем быстро и эффективно находить жилье и у нас нет комиссий.</p>
                                <p>Цена фиксированная и не зависит от стоимости квартиры.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="offer">
        <div class="container">
            <h2>Последние предложения</h2>
            <div class="offer__wrap">
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color blue tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color green tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color yellow tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color blue tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color green tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color yellow tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color blue tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color green tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="offer-item">
                    <div class="offer-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                                <div class="swiper-slide">
                                    <img src="img/slide1.png" alt="">
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <div class="pagination-num">
                                <span>1</span>/<span>23</span>
                            </div>
                        </div>
                        <span class="round-color yellow tooltip" title="проверено модератором"></span>
                    </div>
                    <div class="swiper-text">
                        <div class="swiper-desc">
                            <p>2-к квартира</p>
                            <p>144м2</p>
                            <p>этаж 15/16</p>
                        </div>
                        <div class="swiper-location">
                            <p>Центральный р-н,</p>
                            <p>ул. Никитинская 42</p>
                        </div>
                        <span>35 000 ₽/мес</span>
                        <div class="swiper-text__media">
                            <a href="" class="swiper-heart"></a>
                            <a href="" class="swiper-upload">
                                <img src="img/upload.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="offer-btn">Смотреть все предложения</a>
        </div>
    </div>
    <div class="about-us">
        <div class="container">
            <div class="about-us__title">
                <h2>О нас говорят </h2>
                <div class="swiper-arrows">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
            <div class="about-us__swiper">
                <div class="swiper_second-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="swiper_second-slide">
                                <div class="swiper-slider__user">
                                    <div class="help-right__user">
                                        <img src="img/slider1.png" alt="">
                                        <span></span>
                                    </div>
                                    <div class="swiper-slider_name">
                                        <span>Александр Галка</span>
                                        <div class="rating">
                                            <div class="article__interaction-point-stars">
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                            <span>(4,0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slider__text">
                                    Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает
                                    для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин
                                    оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска
                                    требования.

                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="swiper_second-slide">
                                <div class="swiper-slider__user">
                                    <div class="help-right__user">
                                        <img src="img/slider2.png" alt="">
                                        <span></span>
                                    </div>
                                    <div class="swiper-slider_name">
                                        <span>Валерия Александровна</span>
                                        <div class="rating">
                                            <div class="article__interaction-point-stars">
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                            <span>(4,0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slider__text">
                                    Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения.
                                    Документы с арендодателем я подписала в конце той же недели и сразу переехала.
                                    Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком.
                                    Просто красота!
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="swiper_second-slide">
                                <div class="swiper-slider__user">
                                    <div class="help-right__user">
                                        <img src="img/slider3.png" alt="">
                                        <span></span>
                                    </div>
                                    <div class="swiper-slider_name">
                                        <span>Растислав Сергеевич</span>
                                        <div class="rating">
                                            <div class="article__interaction-point-stars">
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                            <span>(5,0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slider__text">
                                    Поиски квартиры заняли ровно неделю, Андрей проверял все варианты и составлял
                                    подробное описание. Так же оказал помощь при подписании договора. И вот теперь, я
                                    снимаю квартиру без комиссии, от собственника, с хорошим ремонтом, в новом доме по
                                    приемлемой цене.
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="swiper_second-slide">
                                <div class="swiper-slider__user">
                                    <div class="help-right__user">
                                        <img src="img/slider1.png" alt="">
                                        <span></span>
                                    </div>
                                    <div class="swiper-slider_name">
                                        <span>Александр Галка</span>
                                        <div class="rating">
                                            <div class="article__interaction-point-stars">
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                            <span>(4,0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slider__text">
                                    Хорошая услуга ”Личный помощник». За вами закрепляют менеджера, который подбирает
                                    для вас варианты, помогает с подписанием договора ипроверкой квартиры. Константин
                                    оперативно отвечал на наши вопросы и подстраивался под возникающие в ходе поиска
                                    требования.

                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="swiper_second-slide">
                                <div class="swiper-slider__user">
                                    <div class="help-right__user">
                                        <img src="img/slider2.png" alt="">
                                        <span></span>
                                    </div>
                                    <div class="swiper-slider_name">
                                        <span>Валерия Александровна</span>
                                        <div class="rating">
                                            <div class="article__interaction-point-stars">
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#" class="active choosed">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                                <a href="#">
                                                    <svg enable-background="new 0 0 511.998 511.998" height="512"
                                                         viewBox="0 0 511.998 511.998" width="512"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="m414.168 500.62-158.169-83.155-158.169 83.155 30.207-176.121-128.037-124.735 176.86-25.689 79.139-162.697 79.139 162.697 176.86 25.689-128.037 124.734z"
                                                                    fill="#ababab"/>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </div>
                                            <span>(4,0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slider__text">
                                    Отличный сервис! Нужный вариант подобрали уже на следующий день после обращения.
                                    Документы с арендодателем я подписала в конце той же недели и сразу переехала.
                                    Квартира после ремонта, все новое, я — первый арендатор, до центра 10 минут пешком.
                                    Просто красота!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
