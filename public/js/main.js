$(document).ready(function () {

    //btn---active
    $(".help-btn").on("click", function () {
        let index = $(this).parent().index();
        $('.help-btn').removeClass('active');
        $(this).addClass('active');
        $('.help-buttons__text').removeClass('active');
        $('.help-buttons__text').eq(index).addClass('active');
        $('.help__wrap-right').removeClass('active');
        $('.help__wrap-right').eq(index).addClass('active');
    });

    if (document.querySelector('.dropdown')) {
        let buttons = document.querySelectorAll('.dropdown > button');
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener('click', function (e) {
                e.preventDefault();
                this.parentNode.classList.toggle('active');
            });
        }
    }

    if (document.querySelector('.offer-item')) {
        let sliders = document.querySelectorAll('.offer-swiper');
        for (let i = 0; i < sliders.length; i++) {
            let cur = sliders[i].querySelector('.pagination-num > span:first-child');
            let all = sliders[i].querySelector('.pagination-num > span:last-child');
            let count = sliders[i].querySelectorAll('.swiper-slide');
            all.innerHTML = count.length;
            let swiperOffer = new Swiper(sliders[i].querySelector('.swiper-container'), {
                loop: true,
                slidesPerView: 1,
                pagination: {
                    el: sliders[i].querySelector('.swiper-pagination'),
                },
                on: {
                    slideChange: function () {
                        cur.innerHTML = this.realIndex + 1;
                    },
                },
            });
        }
    }

    var swiper = new Swiper('.swiper_second-container', {

        loop: true,
        slidesPerView: 3,
        cssMode: true,
        spaceBetween: 38,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        mousewheel: true,
        keyboard: true,
        breakpoints: {
            1200: {
                slidesPerView: 3,
                spaceBetween: 38
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 38
            },
            939: {
                slidesPerView: 2,
                spaceBetween: 40
            },
            767: {
                slidesPerView: 2,
                spaceBetween: 40
            },
            639: {
                slidesPerView: 1,
                spaceBetween: 40
            },
            576: {
                slidesPerView: 1,
                spaceBetween: 50
            },
            420: {
                slidesPerView: 1,
                spaceBetween: 60
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 60
            }
        },

    });

    var swiper2 = new Swiper('#soc-slider', {
        loop: true,
        spaceBetween: 30,
        effect: 'fade',
    });

    if (document.querySelector('.article__interaction-point-stars')) {
        let starsBlocks = document.querySelectorAll('.article__interaction-point-stars');
        for (let i = 0; i < starsBlocks.length; i++) {
            let stars = starsBlocks[i].querySelectorAll('a');
            var indexStar;
            for (let z = 0; z < stars.length; z++) {
                $(stars[z]).hover(function () {
                    indexStar = $(this).index();
                    for (let y = 0; y <= indexStar; y++) {
                        stars[y].classList.add('active');
                    }
                }, function () {
                    for (let n = 0; n <= indexStar; n++) {
                        stars[n].classList.remove('active');
                    }
                });
                stars[z].addEventListener('click', function (e) {
                    e.preventDefault();
                    let starIndex = $(this).index();
                    for (let d = 0; d <= starIndex; d++) {
                        stars[d].classList.add('choosed');
                    }
                    for (let j = starIndex + 1; j < 5; j++) {
                        stars[j].classList.remove('choosed');
                    }
                });
            }
        }
    }

    //ScrollReveal
    ScrollReveal().reveal('.welcome__wrap, .benefits__wrap, .help__wrap, .offer__wrap, .about-us__swiper, .home .container, .advantages__wrap, .how__Wrap, .user-welcome__title, .agreement-text, .terms-text', {
        scale: 1,
        duration: 1500,
        useDelay: 'onload',
        origin: 'bottom',
        distance: '50px',
        interval: 80,
        mobile: false,
    });


    //////////////////////навигация гайда аренды
    //оберни все дочер пункты в один див и ему давай актив чтоб были видны все
    if ($('.rental-faq')) {

        var scrollTop = 0, rentalBlock = $('.rental-faq__left-block-water');

        function changeTitleMenu(obj) {
            let title = $(obj).html();
            if (title.length >= 13) {
                let title2 = title.trim();
                title2 = title2.substr(0, 13);
                title = title2 + '...';
            }
            $(".rental-faq-mobile-menu-title span").html(title);
        };

        function scrollFunction() {

        };

        $(".rental-faq-mobile-menu-title").on('click', function (event) {
            $(".rental-faq-mobile-menu-title svg").toggleClass('transform');
            $(".rental-faq__left-block-water").toggleClass('show');
        });

        $(".rental-faq-butt").on('click', function (event) {
            changeTitleMenu($(this));

            let shift = 100;
            if (window.innerWidth > 991) shift = 0;

            $('html, body').animate({
                scrollTop: $("#" + $(this).data('rentalFaqId')).offset().top - shift
            }, 800, function () {

            });

            $(".rental-faq-mobile-menu-title svg").removeClass('transform');
            $(".rental-faq__left-block-water").removeClass('show');
        });

        function addActiveFoTabRentalFaq(id) {
            let obj = $("." + id + "-tab");
            if ($(obj).hasClass('active') == false) $(obj).addClass('active');
            changeTitleMenu($(obj));
        }

        function addActiveFoChildrenRentalFaq(id) {
            let obj = $(".children_block_" + id);
            if ($(obj).hasClass('active') == false) $(obj).addClass('active');
        }

        $(window).scroll(function () {
            scrollTop = $(window).scrollTop();

            $(".rental-faq-butt").removeClass('active');

            $(".info-tab__children").removeClass('active');

            $('.rental-faq__container').removeClass('bottom');

            //плавающий режим для левого блока
            $(rentalBlock).removeClass('active');
            $(rentalBlock).removeClass('active2');
            if (scrollTop >= 500 && $(rentalBlock).hasClass('active') == false) $(rentalBlock).addClass('active');

            var sections = $('.rental-faq-content')
                , shift = 200
                , cur_pos = $(this).scrollTop();
            sections.each(function () {
                if (window.innerWidth > 991) {
                    shift = 150;
                }
                var top = $(this).offset().top - shift,
                    bottom = top + $(this).outerHeight();

                if (cur_pos >= top && cur_pos <= bottom) {
                    let val = $(this).attr('id');
                    switch (val) {
                        case 'rental-faq-content1':
                            addActiveFoChildrenRentalFaq('rental-faq-part1');
                            addActiveFoTabRentalFaq('rental-faq-part1');
                            break;
                        case 'rental-faq-content1_1':
                            addActiveFoTabRentalFaq('rental-faq-part1_1');
                            break;
                        case 'rental-faq-content1_2':
                            addActiveFoTabRentalFaq('rental-faq-part1_2');
                            break;
                        case 'rental-faq-content1_3':
                            addActiveFoTabRentalFaq('rental-faq-part1_3');
                            break;
                        case 'rental-faq-content1_4':
                            addActiveFoTabRentalFaq('rental-faq-part1_4');
                            break;
                        case 'rental-faq-content1_5':
                            addActiveFoTabRentalFaq('rental-faq-part1_5');
                            break;
                        case 'rental-faq-content2':
                            addActiveFoChildrenRentalFaq('rental-faq-part2');
                            addActiveFoTabRentalFaq('rental-faq-part2');
                            break;
                        case 'rental-faq-content2_1':
                            addActiveFoTabRentalFaq('rental-faq-part2_1');
                            break;
                        case 'rental-faq-content2_2':
                            addActiveFoTabRentalFaq('rental-faq-part2_2');
                            break;
                        case 'rental-faq-content2_3':
                            addActiveFoTabRentalFaq('rental-faq-part2_3');
                            break;
                        case 'rental-faq-content2_4':
                            addActiveFoTabRentalFaq('rental-faq-part2_4');
                            break;
                        case 'rental-faq-content2_5':
                            addActiveFoTabRentalFaq('rental-faq-part2_5');
                            break;
                        case 'rental-faq-content2_6':
                            addActiveFoTabRentalFaq('rental-faq-part2_6');
                            break;
                        case 'rental-faq-content3':
                            addActiveFoChildrenRentalFaq('rental-faq-part3');
                            addActiveFoTabRentalFaq('rental-faq-part3');
                            break;
                        case 'rental-faq-content3_1':
                            addActiveFoTabRentalFaq('rental-faq-part3_1');
                            break;
                        case 'rental-faq-content3_2':
                            addActiveFoTabRentalFaq('rental-faq-part3_2');
                            break;
                        case 'rental-faq-content4':
                            addActiveFoChildrenRentalFaq('rental-faq-part4');
                            addActiveFoTabRentalFaq('rental-faq-part4');
                            break;
                        case 'rental-faq-content4_1':
                            addActiveFoTabRentalFaq('rental-faq-part4_1');
                            break;
                        case 'rental-faq-content4_2':
                            addActiveFoTabRentalFaq('rental-faq-part4_2');
                            break;
                        case 'rental-faq-content4_3':
                            addActiveFoTabRentalFaq('rental-faq-part4_3');
                            break;
                        case 'rental-faq-content4_4':
                            addActiveFoTabRentalFaq('rental-faq-part4_4');
                            break;
                        case 'rental-faq-content5':
                            addActiveFoChildrenRentalFaq('rental-faq-part5');
                            addActiveFoTabRentalFaq('rental-faq-part5');
                            break;
                        case 'rental-faq-content5_1':
                            addActiveFoTabRentalFaq('rental-faq-part5_1');
                            break;
                        case 'rental-faq-content5_2':
                            addActiveFoTabRentalFaq('rental-faq-part5_2');
                            break;
                        case 'rental-faq-content5_3':
                            addActiveFoTabRentalFaq('rental-faq-part5_3');
                            break;
                        case 'rental-faq-content6':
                            addActiveFoChildrenRentalFaq('rental-faq-part6');
                            addActiveFoTabRentalFaq('rental-faq-part6');
                            break;
                        case 'rental-faq-content6_1':
                            addActiveFoTabRentalFaq('rental-faq-part6_1');
                            break;
                        case 'rental-faq-content6_2':
                            addActiveFoTabRentalFaq('rental-faq-part6_2');
                            break;
                        case 'rental-faq-content6_3':
                            addActiveFoTabRentalFaq('rental-faq-part6_3');
                            break;
                    }
                }
            });
            if (cur_pos > ($('.rental-faq').outerHeight()) && window.innerWidth > 991 && $('.rental-faq__container').hasClass('bottom') == false) {
                $('.rental-faq__container').addClass('bottom');
                $(rentalBlock).removeClass('active');
            }


        });
    }

    //////////////////////
    $(".rate__choose").on('click', function (event) {
        $(".rate__choosen").hide();
        $(".rate__choose").show();
        $(this).hide();
        $(this).parent().find('.rate__choosen').css('display', 'flex');
    });
});
