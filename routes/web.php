<?php

use Illuminate\Support\Facades\Route;

Route::post('auth', 'Main@auth');

Route::post('get-sms-code', 'Main@getCodeForSMS');

Route::middleware(['anonimus'])->group(function () {
    Route::post('login-by-email', 'Auth\LoginController@loginByEmail');
    Route::post('login-by-phone', 'Auth\LoginController@loginByPhone');
    Route::post('register-by-email', 'Auth\RegisterController@registerByEmail');
    Route::post('register-by-phone', 'Auth\RegisterController@registerByPhone');
});

Route::middleware(['authorized'])->group(function () {
    Route::post('logout', 'Auth\LoginController@logout');
});

Route::middleware(['auth'])->group(function () {
    Route::get('logout', 'Auth\LoginController@logoutPage')->name('logout-page');
});

Route::middleware(['guest'])->group(function () {
    Route::get('login', 'Auth\LoginController@loginPage')->name('login-page');
    Route::get('registration', 'Auth\RegisterController@registerPage')->name('registration-page');

    Route::prefix('recovery-password')->group(function () {
        Route::get('/', 'Auth\ForgotPasswordController@index')->name('recovery-password-page');
        Route::post('/', 'Auth\ForgotPasswordController@forgot');

        Route::get('confirmation/{token}', 'Auth\ForgotPasswordController@confirmationPage')->name('recovery-password-confirmation-page');
        Route::post('confirmation', 'Auth\ForgotPasswordController@confirmation');
    });
});

Route::middleware(['admin'])->group(function () {

});

Route::prefix('profile')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::middleware(['auth', 'profile-get'])->group(function () {
            Route::get('/', 'Application\ProfileController@index')->name('profile-page');
        });
        Route::middleware(['authorized', 'profile-post'])->group(function () {
            Route::post('/get-profile', 'Application\ProfileController@getProfile')->name('get-profile');
            Route::post('/change-avatar', 'Application\ProfileController@changeAvatar');
            Route::post('/save-name', 'Application\ProfileController@saveName');
            Route::post('/save-phone', 'Application\ProfileController@savePhone');
            Route::post('/save-password', 'Application\ProfileController@savePassword');
            Route::post('/save-email', 'Application\ProfileController@saveEmail');
            Route::post('/save', 'Application\ProfileController@save');
            Route::prefix('binding')->group(function () {
                Route::prefix('government-services')->group(function () {
                    Route::post('/', 'Application\ProfileController@governmentServicesBinding');
                    Route::post('/cancel', 'Application\ProfileController@governmentServicesBindingCancel');
                });
                Route::prefix('fb')->group(function () {
                    Route::post('/', 'Application\ProfileController@fbBinding');
                    Route::post('/cancel', 'Application\ProfileController@fbBindingCancel');
                });
                Route::prefix('vk')->group(function () {
                    Route::post('/', 'Application\ProfileController@vkBinding');
                    Route::post('/cancel', 'Application\ProfileController@vkBindingCancel');
                });
                Route::prefix('instagram')->group(function () {
                    Route::post('/', 'Application\ProfileController@instagramBinding');
                    Route::post('/cancel', 'Application\ProfileController@instagramBindingCancel');
                });
            });
        });
    });
});

Route::get('/rent-apartment', 'Main@rentApartmentPage')->name('rent-apartment');
Route::get('/rent-room', 'Main@rentRoomPage')->name('rent-room');
Route::get('/terms-of-use', 'Main@termsOfUsePage')->name('terms-of-use');
Route::get('/personal-data-processing', 'Main@personalDataProcessingPage')->name('personal-data-processing');
Route::get('/faq-for-tenants', 'Main@faqForTenantsPage')->name('faq-for-tenants');
Route::get('/faq-for-owners', 'Main@faqForOwnersPage')->name('faq-for-owners');
Route::get('/catalog', 'Main@catalogPage')->name('catalog');
Route::get('/rental-guide', 'Main@rentalGuidePage')->name('rental-guide');
Route::get('/rates', 'Main@ratesPage')->name('rates');
Route::get('/place-an-ad', 'Main@placeAnAdPage')->name('place-an-ad');

Route::prefix('rooms')->group(function () {
    Route::get('/create', 'Main@createRoomPage')->name('create-room-get');
    Route::post('/create', 'Main@createRoom')->name('create-room-post');
    Route::middleware(['auth'])->group(function () {
        Route::get('/{id}/update', 'Main@updateRoomPage')->name('update-room-get');
        Route::get('/{id}/update/{code}', 'Main@repeatUpdateRoomPage')->name('repeat-update-room-get');
    });
});
Route::get('/{room}', 'Main@roomPage')->name('room');

Route::get('/', 'Main@index')->name('home');
